#ifndef __GPMON__
#define __GPMON__
/** @file mon.h
 *  Handling agents and monsters. But we're all monsters, aren't we?
 */
#include "globals.h"
#include <stdint.h>



/// When Something tries to move into something else, there's a variety of reactions
///use Type 1 _____ when colliding with type 2
///hmmmm still ugly, need better names. 
typedef enum
{
  NA,
  ENTERS,
  BLOCKED,
  KILLS,
  IS_KILLED,
  STUNS,
  IS_STUNNED,
  BOTH_STUN,
  PUSHES
}collision_effect_types;


/// Directions that agents can move
/// Note: The DNA makes direct use of this. The values might be important.
///   We could... give them ranges and divvy up 256 amoung them or something.  
typedef enum 
{
  NORTH = 0,
  EAST  = 1,
  SOUTH = 2,
  WEST  = 3,
  ROTATE_CW = 4,
  ROTATE_CCW = 5,
  FORWARD = 6,  
  STRAFERIGHT = 7,
  STRAFELEFT = 8,
  BACKUP = 9,
  REST = 10  //Rest shall always be the last one, because everything past this is synonomous with REST
}direction_types;


///How agent get made. 
typedef enum
{
  ORIGIN_NA,             ///swords   TODO...I think this is stale
  ORIGIN_FRESH_GENESIS,  /// Made in round 1
  ORIGIN_GENESIS,        /// loaded from file
  ORIGIN_CLONE,
  ORIGIN_BRED,          /// The typical one mid-game
  ORIGIN_OTHER          ///Like I wrote him out by hand
}origin_types;

// TODO: treating those dummies and level hazards like any other agent.
///How agent decides what to do
typedef enum
{
  AIALGO_GENETICPROG,    /// The entire point of this whole thing.
  AIALGO_FLEENEAR,       /// Looks at nearby agents runs in the opposit direction. 
}aiAlgo_types;




/// Agents. Both their immediate values, their long-term metrics and a link to their mind where the GP magic happens
/// The mind could honestly be another struct. 
///  Ok, if an index isn't valid, like he just got wiped or something....
struct Agent //TODO naming convention consistancy.  unCap this, fix fallout      Double TODO: wow, no typedef?
{
  int occupied;               ///< if this slot in the array is being used.  Lame side of arrays
  int isDoneWithSoloTrials;   ///< If this agent has been through individual trials.
  char name[AGENTNAMESIZE];   ///< acts as their ID. Consonant-vowel-consonant to be cute. 
  aiAlgo_types aiAlgo;        ///< Genetic Algorithm or a simple hand-made 
  origin_types origin;        ///< Controls type of agent. Also used in accounting and metrics.

  ///Long term metrics that persist between rounds
  int kills;
  int deaths;
  int rounds;
  int numOffspring;

  ///Get cleared between rounds
  int x;
  int y;
  int active;                   ///< Set up in trial, cleared when it dies or otherwise should be skipped over
  direction_types facing;
  int fitness; ///< For whatever specific issue I'm currently tracking
  int cycles;   ///< used by gplib.... Which is probably a BAD case of coupling. TODO
  int opcodeUsage[NUMOPCODES];  ///metric

  //MIND WORD SIZE is now 16-bit. 
  uint16 mind[MIND_SIZE]; ///< Where all the GP magic happens.  No longer a pointer to dynamic land. Because that was stupid as it was ALWAYS MIND_SIZE
};


/// OK, so I need to walk throgh a random collection of the agents, but I don't want to actually shuffle data around. 
/// Gimme a shuffle of indexes. No repeats. For a specific size.  DON'T make that size too big. 
/// And make it only occupied indexes of g_mon
void shuffleMonList(int i[], int sizeofList, int* numViable);





extern struct Agent g_mon[];  ///< Yeah, kinda lame. But there's just the one. It keeps track of all the agents we're working with.   Long-term: sent to file, being tested: g_map

extern int g_numAgents;

void initMon();                   ///< loads monsters from file or generates new agents
void agentResetRound();

/** Moves an agent. Note, we only give the x,y and which way to go. Which is shit. 
 * LOOOOOTS of shenanigians with moving swords. Used to be swords were their own agent. That changed.
 * Deals with the effects of objects running into each other. 
 * @return True if move completes
 */
char agentMove(agentIndex i, direction_types direction);  
void agentDetain(agentIndex i);  ///< Literally moves them no a special square where they don't get processed. Yes margeret, there is a hell.
void agentKill(agentIndex i); ///< Used to actually delete agents, now it detains them.
void agentStun(agentIndex i); ///< plays with their turn so they don't process the next turn. It's supposed to discourage sword people running into shields.
void agentRotate(agentIndex i, direction_types direction);  ///< Juuuust handles the agent's facing


// Swords used to be their own agent
//char equip(struct Agent* agent);  
//char equipGenesis(int x, int y, tile_types equip);
//void unequip(int x, int y);
void moveDir(agentIndex i, int* x, int* y, direction_types direction);  ///< Actually moves an agent. 


int headCount();  ///< For debugging. Just counts things in the map. 


/** Makes a new agent from scratch. Between this and loadAgent, this is how g_mon gets populated
 * @param placement How it goes onto the map, if at all. 
 * @param x  If placement is specific, put here
 * @param y  If placement is specific, put here
 * @param facing  If placement is specific, put here
 * @param agent Slot of g_mon to put agent into, or first available if NO_AGENT
 * @param isBraindead  If we should skip making a random mind, when we breed a new baby
 */
agentIndex generateAgent(placement_types placement, int x, int y, int facing, agentIndex agent, char isBraindead); 
agentIndex agentGetFirstOpenSlot(); ///< where to place new agents. Downside of switching to a big-ass array. Youv'e got to keep track of stuff. 

#endif
