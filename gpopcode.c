

#include "gpopcode.h"
#include "map.h"
#include "gpfight.h"
#include "logging.h"
#include "globals.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

extern int g_consiousnessStream;
extern int g_processingLimit;

//uuuhhh... doesn't exactly belong here. Need to move it to... what? A globals.h?  No, .h is not for declarations... TODO
const char* g_opcodeList[] = 
{
  "OP_NOP       :",
  "OP_MOV       :",
  "OP_ADD       :",
  "OP_SUB       :",
  "OP_MULT      :",
  "OP_DIV       :",
  "OP_MOD       :",
  "OP_CMP       :",
  "OP_JEQL      :",
  "OP_JNEQL     :",
  "OP_JLESS     :",
  "OP_JMORE     :",
  "OP_LOGAND    :",
  "OP_LOGOR     :",
  "OP_BITAND    :",
  "OP_BITOR     :",
  "OP_BITXOR    :",
  "OP_BITNOT    :",
  "OP_IN_LOOK   :",
  "OP_OUT_MOVE  :",
  "OP_IN_RAND   :",
  "OP_IN_FACING :",  //This and below aren't supported yet
  "OP_IN_HOLDING:",
  "OP_OUT_GRAB  :",
  "OP_OUT_SAY   :",
  "OP_IN_LISTEN :"
};



//This is essentially our mind-code interperator.
//Output: The direction of movement, includes REST.
// MINDWORDSIZE is now uint16
uint16 mindCrunch(agentIndex agent)
{
  int cycle = 0;
  uint16* mind;
  int x, y;
  uint16 op, pc, arg1, arg2, arg1Flag, arg2Flag;
  char tmp[20];
  int i;
  char endTurn = 0;
  uint16 result = REST; //by default, we simply sit there

  mind = g_mon[agent].mind;
  x = g_mon[agent].x;
  y = g_mon[agent].y;

  //Part of consiousness Stream
  if(g_consiousnessStream)
  {  
    logging(LOG_CONSIOUSNESS ,"%s's turn @%d,%d facing: %d\n", g_mon[agent].name, g_mon[agent].x, g_mon[agent].y, g_mon[agent].facing);
    logging(LOG_CONSIOUSNESS ,"PC\top\targ1\targ2\tmind[0]\tmind[1]\tmind[2]\tmind[3]\tmind[4]\tmind[5]\tmind[6]\tmind[7]\n");
  }
  while(cycle++ < g_processingLimit && !endTurn)
  {  
    pc = mind[ADDRESS_PC]; 

    if(pc >= MIND_SIZE-3)  //With self-writing code it can set up a fail and get on the edge with a three-part op
    {
      pc = CODE_START;
      mind[ADDRESS_PC] = CODE_START;
    }  

    if(g_step && g_stepConsciousness)
    {
      userObserve();
    }    

    //increment PC before we do anything else, like jump around
    mind[ADDRESS_PC] += 3;

    //Accounting
    g_mon[agent].cycles++;


    // Get the op-code  
    op = mind[pc]  & 0x3fff; //Yep, those first two bits are magic, drop em.

    // Get the first argument
    arg1Flag = mind[pc] & ARG1IMM;
    if( arg1Flag != 0)   
    {
      arg1 = mind[pc+1]; //Immediate Arg1
    }
    else
    {
      arg1 = mind[mind[pc+1]];  //Reference Arg1
    }  

    // Get the second argument
    arg2Flag = mind[pc] & ARG2IMM;
    if( arg2Flag != 0)
    {
      arg2 = mind[pc+2];
    }
    else
    {
      arg2 = mind[mind[pc+2]];
    }  

    if(g_consiousnessStream)
    {
      //Stream of consiousness,   Crazy useful for debugging.
      //Formatting is still a bit of a bitch
      getOpCodeName(op, tmp, 20);
      logging(LOG_CONSIOUSNESS ,"%3d:\t%s\t",pc,tmp);    //PC and opcode
      if(arg1Flag != 0)            
        logging(LOG_CONSIOUSNESS ,"%3d\t",arg1);    //Arg1 immediate
      else
        logging(LOG_CONSIOUSNESS ,"%3d->%3d\t",mind[pc+1],arg1); //Arg1 ref
      if(arg2Flag != 0)
        logging(LOG_CONSIOUSNESS ,"%3d\t",arg2);    //Arg2 immediate
      else
        logging(LOG_CONSIOUSNESS ,"%3d->%3d\t",mind[pc+2],arg2); //Arg2 ref

      for(i=0; i<CODE_START; i++)
        logging(LOG_CONSIOUSNESS ,"%3d\t",mind[i]);    //Registers
      logging(LOG_CONSIOUSNESS ,"\n");
      //formatMind(char op, char arg1, char arg2, char* ret, ) ...now why is this commented out? I was such shit at keeping my own shit clean...
    }

    //Metrics
    if( op > NUMOPCODES)
      g_mon[agent].opcodeUsage[0]++;
    else
      g_mon[agent].opcodeUsage[op]++;

    //DO IT  
    switch(op)
    {
      default:
      case   OP_NOP:
        ;//doing nothin
        break;
      case   OP_MOV:  //Odd one with arg1, double check that it's doing what MOV normally does
        mind[arg1] = arg2;
        break;
      case   OP_ADD:
        mind[ADDRESS_AX] = arg1 + arg2;
        break;
      case   OP_SUB:
        mind[ADDRESS_AX] = arg1 - arg2;
        break;
      case   OP_MULT:
        mind[ADDRESS_AX] = (uint16) (arg1 * arg2);
        break;
      case   OP_DIV:
        if(arg2 != 0)
          mind[ADDRESS_AX] = arg1 / arg2;
        break;
      case  OP_MOD:
        if(arg2 != 0)
          mind[ADDRESS_AX] = arg1 % arg2;
        break;  

      case  OP_CMP:
        if(arg1 < arg2)
          mind[ADDRESS_AX] = -1;
        if(arg1 == arg2)
          mind[ADDRESS_AX] = 0;
        if(arg1 > arg2)
          mind[ADDRESS_AX] = 1;
        break;

      case   OP_JEQL:
        if(arg1 == arg2)
        {
          mind[ADDRESS_PC] = mind[ADDRESS_AX];
        }

        break;
      case   OP_JNEQL:
        if(arg1 != arg2)
        {
          mind[ADDRESS_PC] = mind[ADDRESS_AX];
        }
        break;
      case   OP_JLESS: 
        if(arg1 < arg2)
        {
          mind[ADDRESS_PC] = mind[ADDRESS_AX];
        }
        break;
      case   OP_JMORE:
        if(arg1 > arg2)
        {
          mind[ADDRESS_PC] = mind[ADDRESS_AX];
        }
        break;
        /*case   OP_JMP:
          mind[ADDRESS_PC] = mind[ADDRESS_AX];
          break;*/

      case   OP_LOGAND:
        if(arg1 && arg2)
          mind[ADDRESS_AX] = 1;
        else
          mind[ADDRESS_AX] = 0;
        break;
      case   OP_LOGOR:
        if(arg1 || arg2)
          mind[ADDRESS_AX] = 1;
        else
          mind[ADDRESS_AX] = 0;
        break;

      case  OP_BITAND:
        mind[ADDRESS_AX] = arg1 & arg2;
        break;
      case  OP_BITOR:
        mind[ADDRESS_AX] = arg1 | arg2;
        break;
      case  OP_BITXOR:
        mind[ADDRESS_AX] = arg1 ^ arg2;
        break;
      case  OP_BITNOT:
        mind[ADDRESS_AX] = !arg1;
        break;      

      case   OP_IN_LOOK:
        //Originally they could look anywhere baed on ax and bx. 
        //mind[ADDRESS_AX] = lookForward(x,y, g_mon[agent].facing); //...wait a tick, they hold swords, this will always return '!'
        mind[ADDRESS_AX] = lookAround(x,y, g_mon[agent].facing, arg1, arg2); 
        break;
      case   OP_OUT_MOVE:
        result = mind[ADDRESS_AX];
        endTurn = 1;          //Ends turn
        break;
      case   OP_IN_RAND:
        mind[ADDRESS_AX] = rand();
        break;
      case  OP_IN_FACING:
        mind[ADDRESS_AX] = g_mon[agent].facing;
        break;
      case  OP_OUT_SAY: //uh...
        break;
      case  OP_IN_LISTEN:
        break;

    }

  }
  //So they timed out and thought too much without doing anything, let's punish them. 
  if(!endTurn)
  {
    g_mon[agent].fitness--;
  }
  return result;
}





//Translate text of opcode to the opcode number
op_codes getOpCodeNumber(char* name)
{
  op_codes i;
  for(i=0; i< NUMOPCODES; i++)
  {
    if(strcmp(name,g_opcodeList[i]) == 0)
      return i;
  }
  return OP_NOP;
}

//Translate opcode number to the text name,
void getOpCodeName(op_codes op, char* name, int nameSizeInBytes)
{
  op = 0x3FFF & op; //0b0011111111111111 

  if(op > NUMOPCODES)
    snprintf(name, nameSizeInBytes, "OP_NOP~      :");
  else
    snprintf(name, nameSizeInBytes, "%s",g_opcodeList[op]);
  return;
}


//To be called in userObserve, paused with an agent selected
void displayMind(agentIndex agent, int(*dispFunc)(int, int, const char*, ...) )
{
  const int dispX = 40; //Top left starting point for this. 
  const int dispY = 4;
  char buff[100];
  if(agent == NO_AGENT) return;
  uint16* mind = g_mon[agent].mind;  
  int pc = mind[ADDRESS_PC];

  //mvprintw(dispY,dispX,"PC: %d AX: %d",pc,mind[ADDRESS_AX]);
  // oh right, no curses functions down here.   ...Call something from gpfight?
  //   or how about passing along what we want it to call?
  //     ... holy shit, that worked!   And it worked surprisingly well.
  snprintf(buff,100,"PC: %d AX: %d",pc,mind[ADDRESS_AX]);
  (*dispFunc)(dispY,dispX,buff);

  // Opcode debugging screen. 
  for(int i=0; i<20; i++)
  {
    //TODO: don't overrun mind-size and display XXX instead or something
    char opcodeName[50];
    int arg1 = mind[pc+1] & 0b01111111; 
    int arg2 = mind[pc+2] & 0b01111111; 
    char arg1Ref =  (mind[pc+1] & 0b10000000)? '@' : ' '; 
    char arg2Ref =  (mind[pc+2] & 0b10000000)? '@' : ' '; 
    getOpCodeName(mind[pc], opcodeName, 50);
    snprintf(buff, 100, "%03d %s %c%d %c%d", pc, opcodeName, arg1Ref, arg1, arg2Ref, arg2);
    (*dispFunc)(dispY+1+i,dispX,buff);
    pc = pc + 3;
  }

}

