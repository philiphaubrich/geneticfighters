#ifndef __PHILLOGGING__
#define __PHILLOGGING__
/** @file Logging. Small and sweet, but a good thing to have out on it's own.
 *
*/

#include <stdarg.h>
#include <stdio.h>

///Bitfield of various logging depths. An attempt to stop getting massive log files of stuff I didn't care about
typedef enum
{
  LOG_NONE = 0x00,            ///< Log nothing. The log file is never written to
  LOG_ALWAYS = 0xFFFF,        ///< Log everything. That is, every logging() call actually makes it to the log
  LOG_ERR = 0x0001,           ///< Errors and shit that breaks
  LOG_CONSIOUSNESS = 0x0002,  ///< That's every step through the DNA, every thought the agents have. 
  LOG_MIDGAME = 0x0004,       ///< mid-round game actions, deaths/kills/sword-grabs/whatnot
  LOG_FITNESS = 0x0008,       ///< fitness functions, Detailing who gets selected, who breeds, who gets born whatnot
  LOG_HIGHGAME = 0x0010,      ///< Higher-level game info. Rounds, who gets loaded, who gets deleted, 
  LOG_VERBOSE = 0x0020        ///< Eh, everything else
}logging_types;


/// Yeah, a global setting for what we log.  fuck getters and setters.
/// So there has to be a bit match between THIS variable, and what the logging command is passed
/// Bit-wise-and the logging_types if you need.
extern int g_logging;

/// Opens the log file
int logInit();
void logfflush(); ///< fflush the log. I just don't want to expose any more globals than needbe
void logClose();  ///< close the file

/** Wrapping fprintf(f_log  so we can turn off logging when we want to.
  Wooo! variadic funtions!
  Assumes we're outputting to f_log
  Added type in for shits'n'giggles: ERR, gameplay, consiousnessStream, 
  @param type     What sort of log message you're making. 
  @param format   Matches printf's format
  @param ...      Matches printf's usage
 */
int logging(logging_types type, const char* format, ...); 




#endif
