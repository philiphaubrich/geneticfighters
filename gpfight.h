#ifndef __GPFIGHT__
#define __GPFIGHT__
/* 	basic.h
    9/14/11 By Philip Haubrich
	gpfight.h 
	4/10/12 By Philip Haubrich
*/
#include "globals.h"
#include "map.h"
#include <time.h>


//These I want to get rid of or trim
extern int g_numRounds;
extern int g_numAgents;
extern int g_numRounds;
extern int g_roundLength;
extern int g_trialLength;
extern int g_processingLimit;
extern int g_trialsForAgent;
extern int g_consiousnessStream;
extern int g_step;
extern int g_stepConsciousness;
extern clock_t g_speed;
#define QUARTERSEC  CLOCKS_PER_SEC/4      // -(s)low also user-view time
#define MILLISEC    CLOCKS_PER_SEC/1000   // The default
#define NO_DISPLAY  -1                    // -(d)ark
#define FASTEST      0                    // -(f)asest



void init();                      ///< Sets up curses if !NO_DISPLAY, srand, opens log and chart.txt
void initRound();                 ///< Loads agents from file, makes new agents 

void doit();
void advanceTime(int* time, int agentList[], int listSize); ///< Do whatever it is the game is going to do. Processses everyone's turn

/** Mid battle user-is-looking-at-shit mode
 *  It has its own big loop, paint, and user-interface.
 *  Enters into it with g_step and user entering ' '
 */
void userObserve();

/** Handles end-round metrics, calls the fitnessTests and selection. 
 *  chartLog
 *  @return The average fitness of the round    TODO: conside switching this to max fitness of population
 */
void endRound(int* lastBest, int* avg); 
void quitProgram(); ///< wraps up stuff. Files, curses stuff
void advanceTime();  ///< A big tick in the game. Each agent gets to process their mind and choose an action, and move


#endif 


