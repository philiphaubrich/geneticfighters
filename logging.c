
#include "logging.h"
#include "globals.h"

#include <stdio.h>
#include <stdarg.h>

/// Yeah, a global setting for what we set.  fuck getters and setters.
int g_logging = LOGGING_DEFAULT;  

FILE* f_log;

int logInit()
{
  f_log = fopen("log.txt", "wa");
  if(f_log == NULL)
     return -1;
  return 0;
}

void logfflush()
{
  fflush(f_log);
}

void logClose()
{
  fflush(f_log);
  fclose(f_log);
}


//Wrapping fprintf(f_log  so we can turn off logging when we want to.
//Wooo! variadic funtions!
//Added type in for shits'n'giggles: ERR, gameplay, consiousnessStream, 
int logging(logging_types type, const char* format, ...)
{
  int ret;
  va_list args;

  if(!(g_logging & type)) //mask
    return -1;

  va_start (args, format);
  ret = vfprintf (f_log, format, args); //Yep, well this was specifically written for wrappers.
  va_end (args);

  return ret;
}

