//Ok, Phil's very first real attempt at unit testing. Let's see how this goes.

//Also,  splint *.c -preproc -DDIR=int
//or  splint *.c -preproc -DDIR=int -type -predboolint -incondefs -nullassign -retval -boolops -globstate -unrecog -dependenttrans +charindex -predboolothers 


#include "minUnit.h"
#include "gpfight.h"
#include "gplib.h"
#include "gpopcode.h"
#include "globals.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int tests_run = 0;

/*

 */

/*
   static char * test_()
   {

   return 0;
   }
 */

static char * test_inMap()
{
  //Hmmmmm, each one of these will return... 
  mu_assert("inMap 0 0 isn't", inMap(0,0) == 1);
  mu_assert("inMap 1 0 isn't", inMap(1,0) == 1);
  mu_assert("inMap 0 1 isn't", inMap(0,1) == 1);
  mu_assert("inMap -1 -1", inMap(-1,-1) == 0);
  mu_assert("inMap max", inMap(MAP_SIZE_X-1,MAP_SIZE_Y-1) == 1);
  mu_assert("inMap max", inMap(MAP_SIZE_X-1,MAP_SIZE_Y) == 0);
  mu_assert("inMap max", inMap(MAP_SIZE_X,MAP_SIZE_Y-1) == 0);
  mu_assert("inMap max", inMap(MAP_SIZE_X,MAP_SIZE_Y) == 0);
  return 0;
}

static char * test_generateMap()
{
  int x,y;
  int count=0;

  generateMap(MAP_EMPTY);

  for(x=0; x < MAP_SIZE_X; x++)
  {
    for(y=0; y < MAP_SIZE_Y; y++)
    {
      mu_assert("mapGenerate, wtf is this agent doing here?", g_map[x][y].agentPointer == NULL);
      mu_assert("mapGenerate, wtf is this tile doing here?", g_map[x][y].tile == TILE_BLANK);
    }
  }

  generateMap(MAP_SWORD_PERIMITER);
  for(x=0; x < MAP_SIZE_X; x++)
  {
    for(y=0; y < MAP_SIZE_Y; y++)
    {
      mu_assert("mapGenerate, wtf is this agent doing here? sword perimiter", g_map[x][y].agentPointer == NULL);
      if(g_map[x][y].tile == TILE_SWORD)
        count++;
    }
  }
  mu_assert("mapGenerate, weird number of swords", count == 2*MAP_SIZE_X+2*MAP_SIZE_Y-4);

  generateMap(MAP_RANDOM_WALLS);
  for(x=0; x < MAP_SIZE_X; x++)
  {
    for(y=0; y < MAP_SIZE_Y; y++)
    {
      mu_assert("mapGenerate, wtf is this agent doing here? randomWalls", g_map[x][y].agentPointer == NULL);
      mu_assert("mapGenerate, wtf is this tile doing here?", g_map[x][y].tile == TILE_WALL || g_map[x][y].tile == TILE_BLANK);
    }
  }  
  return 0;
}

static char * test_generatingAgent2()
{
  struct Agent* agent;

  //generate a clear map
  generateMap(MAP_EMPTY);

  agent = generateAgent2(0,0,EAST);
  mu_assert("generateAgent2", agent->x == 0);
  mu_assert("generateAgent2", agent->y == 0);
  mu_assert("generateAgent2 Couldn't put tile on map", g_map[0][0].tile == TILE_DUDE );
  mu_assert("generateAgent2 Couldn't put agent on map", g_map[0][0].agentPointer == agent );
  mu_assert("generateAgent2 Couldn't put sword on map", g_map[1][0].tile == TILE_SWORD );
  mu_assert("generateAgent2", g_map[0][1].tile == TILE_BLANK );
  mu_assert("generateAgent2", g_map[1][1].tile == TILE_BLANK );
  mu_assert("generateAgent2, mind null", agent->mind == NULL );
  mu_assert("generateAgent2 has friends?", headCount() == 1);

  free(agent);

  agent = generateAgent2(5,7,NORTH);
  mu_assert("generateAgent2", agent->x == 5);
  mu_assert("generateAgent2", agent->y == 7);
  mu_assert("generateAgent2 Couldn't put tile on map", g_map[5][7].tile == TILE_DUDE );
  mu_assert("generateAgent2 Couldn't put agent on map", g_map[5][7].agentPointer == agent );
  mu_assert("generateAgent2 Couldn't put sword on map", g_map[5][6].tile == TILE_SWORD );
  mu_assert("generateAgent2", g_map[5][8].tile == TILE_BLANK );
  mu_assert("generateAgent2", g_map[4][7].tile == TILE_BLANK );
  mu_assert("generateAgent2", g_map[6][7].tile == TILE_BLANK );
  mu_assert("generateAgent2, mind null", agent->mind == NULL );

  free(agent);

  agent = generateAgent2(0,7,WEST);
  mu_assert("generateAgent2 Sword would need to go into OOB, fails", agent == NULL);

  agent = generateAgent2(5,7,WEST); 
  mu_assert("generateAgent2 can't place someone over someone else", agent == NULL);  

  agent = generateAgent2(6,7,WEST); 
  mu_assert("generateAgent2 can't place swords over someone else", agent == NULL);  

  agent = generateAgent2(6,6,WEST); 
  mu_assert("generateAgent2 can't place swords over someone's else's", agent == NULL);  

  return 0;
}

static char * test_generatingAgent()
{
  struct Agent* agent1;

  //Ok, make a clean map
  generateMap(MAP_EMPTY);
  //Generate one agent.

  agent1 = generateAgent(PLACEMENT_RAND);

  //Verify we have a pointer
  mu_assert("generateAgent fails", agent1 != NULL);

  //verify there's just the one one the map and the tile and pointers are right
  mu_assert("generateAgent fails", headCount() == 1);
  mu_assert("generateAgent put it someplace odd", g_map[agent1->x][agent1->y].agentPointer == agent1);
  mu_assert("generateAgent put in an odd tile", g_map[agent1->x][agent1->y].tile == TILE_DUDE);

  //TODO   Finish generatingAgent 
  //verify it has a mind
  //Verify it has a spread of material in it's mind  (Or test this seperately with a test of GenRandMind?)

  //test collisions somehow
  //Test no availiable spots
  return 0;
}


char loadMind_Mock(FILE* f_file, uint16* mind)
{
  int i;
  for(i=0; i < MIND_SIZE; i++)
  {
    mind[i] = i;
  }
  return 0;
}

static char * test_agentFileIO()
{
  struct Agent* agent1;
  struct Agent* agent2;
  char filename[AGENTNAMESIZE];

  generateMap(MAP_EMPTY);

  //TODO  Unit testing between windows and linux   file IO
  //Ok, this is going to be annoying or weak considering the extra hurdle of dealing with the different way files are handled between windows and linux.
  //ideally, we would cut out the actual file IO bit and test if the structure is good, and then test IO...
  //hmmmm

  // script to clear all agent?
  // Create one .agent file
  //#define loadMind loadMind_Mock //TODO: yeahhhh, this ain't workin...
  agent1 = generateAgent(PLACEMENT_RAND);
  saveAgent(agent1,NULL);
  snprintf(filename, AGENTNAMESIZE, "agents/%s.agent",agent1->name);
  agent2 = loadAgent(filename, NULL, PLACEMENT_RAND);
  //#undef loadMind

  //assert the thing we loaded is the same thing we saved.
  //mu_assert("generateAgent put in an odd tile", equal(agent1, agent2));
  mu_assert("Agent can't save and load it's name", strcmp(agent1->name, agent2->name) == 0);
  mu_assert("Agent can't save and load it's team", agent1->team == agent2->team);
  mu_assert("Agent can't save and load it's kills", agent1->kills == agent2->kills);
  mu_assert("Agent can't save and load it's deaths", agent1->deaths == agent2->deaths);
  mu_assert("Agent can't save and load it's rounds", agent1->rounds + 1 == agent2->rounds); //Increment

  mu_assert("Agent can't save and load it's mind", memcmp(agent1->mind, agent2->mind, MIND_SIZE) == 0); 



  return 0;
}


//char agentMove(int x, int y, direction_types direction)
static char * test_agentMove()
{
  struct Agent* agent1;
  struct Agent* agent2;
  struct Agent* agent3;

  generateMap(MAP_EMPTY);

  g_map[1][1].tile = TILE_WALL;
  g_map[0][2].tile = TILE_WALL;
  g_map[2][3].tile = TILE_WALL;



  agent1 = generateAgent2(0,0,SOUTH);
  agent2 = generateAgent2(1,0,EAST);
  agent3 = generateAgent2(2,2,NORTH);
  /*
     /----
     |@@!
     |!#!
     |# @
   */

  mu_assert("error, moving agent OOB, ret", agentMove(0,0, NORTH) == 0);
  mu_assert("error, moving agent OOB, x", agent1->x == 0);
  mu_assert("error, moving agent OOB, y", agent1->y == 0);
  mu_assert("error, moving agent OOB, facing", agent1->facing == SOUTH);
  mu_assert("error, moving agent OOB, swordtile", g_map[0][1].tile == TILE_SWORD);
  mu_assert("error, moving agent OOB, dudetile", g_map[0][0].tile == TILE_DUDE);
  mu_assert("error, moving agent OOB, pointer", g_map[0][0].agentPointer = agent1);
  //Pointer isn't somewhere else...?

  mu_assert("error, moving agent OOB", agentMove(0,0, WEST) == 0);
  mu_assert("error, moving agent OOB, x", agent1->x == 0);
  mu_assert("error, moving agent OOB, y", agent1->y == 0);
  mu_assert("error, moving agent OOB, facing", agent1->facing == SOUTH);
  mu_assert("error, moving agent OOB, swordtile", g_map[0][1].tile == TILE_SWORD);
  mu_assert("error, moving agent OOB, dudetile", g_map[0][0].tile == TILE_DUDE);
  mu_assert("error, moving agent OOB, pointer", g_map[0][0].agentPointer = agent1);

  mu_assert("error, moving agent into another", agentMove(0,0, EAST) == 0);
  mu_assert("error, moving agent into another, x", agent1->x == 0);
  mu_assert("error, moving agent into another, y", agent1->y == 0);
  mu_assert("error, moving agent into another, facing", agent1->facing == SOUTH);
  mu_assert("error, moving agent into another, swordtile", g_map[0][1].tile == TILE_SWORD);
  mu_assert("error, moving agent into another, dudetile", g_map[0][0].tile == TILE_DUDE);
  mu_assert("error, moving agent into another, pointer", g_map[0][0].agentPointer = agent1);

  mu_assert("error, rotating agent OOB", agentMove(0,0, ROTATE_CW) == 0);
  mu_assert("error, rotating agent OOB, x", agent1->x == 0);
  mu_assert("error, rotating agent OOB, y", agent1->y == 0);
  mu_assert("error, rotating agent OOB, facing", agent1->facing == SOUTH);
  mu_assert("error, rotating agent OOB, swordtile", g_map[0][1].tile == TILE_SWORD);
  mu_assert("error, rotating agent OOB, dudetile", g_map[0][0].tile == TILE_DUDE);
  mu_assert("error, rotating agent OOB, pointer", g_map[0][0].agentPointer = agent1);

  mu_assert("error, backing agent3 into wall", agentMove(2,2, SOUTH) == 0);
  mu_assert("error, backing agent3 into wall, x", agent3->x == 2);
  mu_assert("error, backing agent3 into wall, y", agent3->y == 2);
  mu_assert("error, backing agent3 into wall, facing", agent3->facing == NORTH);
  mu_assert("error, backing agent3 into wall, swordtile", g_map[2][1].tile == TILE_SWORD);
  mu_assert("error, backing agent3 into wall, dudetile", g_map[2][2].tile == TILE_DUDE);
  mu_assert("error, backing agent3 into wall, pointer", g_map[2][2].agentPointer = agent3);

  mu_assert("error, moving agent's sword into wall", agentMove(0,0, SOUTH) == 0);
  mu_assert("error, moving agent's sword into wall, x", agent1->x == 0);
  mu_assert("error, moving agent's sword into wall, y", agent1->y == 0);
  mu_assert("error, moving agent's sword into wall, facing", agent1->facing == SOUTH);
  mu_assert("error, moving agent's sword into wall, swordtile", g_map[0][1].tile == TILE_SWORD);
  mu_assert("error, moving agent's sword into wall, dudetile", g_map[0][0].tile == TILE_DUDE);
  mu_assert("error, moving agent's sword into wall, pointer", g_map[0][0].agentPointer = agent1);

  mu_assert("error, moving agent3's sword into wall", agentMove(2,2, WEST) == 0);
  mu_assert("error, moving agent3's sword into wall, x", agent3->x == 2);
  mu_assert("error, moving agent3's sword into wall, y", agent3->y == 2);
  mu_assert("error, moving agent3's sword into wall, facing", agent3->facing == NORTH);
  mu_assert("error, moving agent3's sword into wall, swordtile", g_map[2][1].tile == TILE_SWORD);
  mu_assert("error, moving agent3's sword into wall, dudetile", g_map[2][2].tile == TILE_DUDE);
  mu_assert("error, moving agent3's sword into wall, pointer", g_map[2][2].agentPointer = agent3);

  mu_assert("error, rotating agent's sword into wall", agentMove(1,0, SOUTH) == 0);
  mu_assert("error, rotating agent's sword into wall, x", agent2->x == 1);
  mu_assert("error, rotating agent's sword into wall, y", agent2->y == 0);
  mu_assert("error, rotating agent's sword into wall, facing", agent2->facing == EAST);
  mu_assert("error, rotating agent's sword into wall, swordtile", g_map[2][0].tile == TILE_SWORD);
  mu_assert("error, rotating agent's sword into wall, dudetile", g_map[1][0].tile == TILE_DUDE);
  mu_assert("error, rotating agent's sword into wall, pointer", g_map[1][0].agentPointer = agent2);

  mu_assert("error, moving agent's sword into sword", agentMove(2,2, NORTH) == 0);
  mu_assert("error, moving agent's sword into wall, x", agent3->x == 2);
  mu_assert("error, moving agent's sword into wall, y", agent3->y == 2);
  mu_assert("error, moving agent's sword into wall, facing", agent3->facing == NORTH);
  mu_assert("error, moving agent's sword into wall, swordtile", g_map[2][1].tile == TILE_SWORD);
  mu_assert("error, moving agent's sword into wall, dudetile", g_map[2][2].tile == TILE_DUDE);
  mu_assert("error, moving agent's sword into wall, pointer", g_map[2][2].agentPointer = agent3);

  //TODO more testing with how everything can and can't move.

  return 0;
}

static char * all_tests() 
{
  //Testing basics, super basics
  mu_run_test(test_inMap);
  mu_run_test(test_generateMap);
  //Testing agents
  mu_run_test(test_generatingAgent2);
  mu_run_test(test_generatingAgent);
  mu_run_test(test_agentFileIO);

  //mu_run_test(test_loadAgent);

  //Testing map action
  mu_run_test(test_agentMove);

  //Testing out minds

  //Testing out selection

  return 0;
}

//int mainTest(int argc, char **argv) 
int test()
{
  char *result = all_tests();
  if (result != 0) 
  {
    printf("%s\n", result);
  }
  else 
  {
    printf("ALL TESTS PASSED\n");
  }
  printf("Tests run: %d\n", tests_run);

  return result != 0;
}




