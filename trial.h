#ifndef __GPTRIAL__
#define __GPTRIAL__
/** @file trial 
 * So I want to be able to set up sequential tests, which get progressivly harder so I can train up agents. 
 * Give them more of a slope than a cliff.
 * And, just a reminder, there are individual trials, and then the group trial (what was the standard run prior) is now just another trial
*/


#include "gpfight.h"

#define TRIAL_NAME_SIZE 20

#define NUM_TRIALS 4

#define MAX_POSSIBLE_FITNESS 5000


typedef enum
{
  TRIAL_TYPE_SOLO,
  TRIAL_TYPE_COMPETATIVE,
}TrialType_t;

typedef enum
{
  TRIAL_STATIC_PEASANTS,
  TRIAL_RAND_PEASANT,
  TRIAL_FLEEING_PEASANT,
  TRIAL_DEATHMATCH,
}TrialId_t;

typedef enum
{
  TRIAL_OUTCOME_CONTINUE,  // Keep going onto next trial. 
  TRIAL_OUTCOME_ABORT,     // if it doesn't pass this trial, give up on it. 
} TrialOutcome_t;

/// Adding various types of trials to perform sequentially
struct Trial
{
  char name[TRIAL_NAME_SIZE];
  TrialId_t id;
  TrialType_t type;
  map_types map;
  int roundLength;
  int processingTime;
  int numAgents;
  placement_types placement;
  TrialOutcome_t outcome;
  int (*fitnessFuncPtr)(agentIndex, TrialOutcome_t* outcome);
  // possible TODO: Also be nice to know the end-state. Like, kill the dude and the trial is over.
};


extern struct Trial g_trials[NUM_TRIALS];

int initTrial();

void setupTrialAgent(agentIndex agent, int trial);
void setupTrialMap(int trial, int* trialLength);
void endTrial(agentIndex agentInTrial, int trial);
//void postTrialCheck(agentIndex agentInTrial);
#endif
