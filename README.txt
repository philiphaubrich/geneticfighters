Arguments:
[-a numAgents]  How many agents to generate. Default: 15
[-r numRounds] How many rounds to process. Program will quit afterwards. Default: 10   0 for infinate
[-l roundLength] Number of turns in a round.
[-p maxCycles] How many instructions to let them crunch before forcing an end of round.
[-f] Run fast. Limited only by your processor. Default limits turns to 1 millisecond   (Bug in linux)
[-s] Run slow. Turn is limited to .25 seconds. Default limits turns to 1 millisecond
[-d] Run dark. No input or output. Runs really fast though.  
[-c] Log consiousness stream. Every opcode is logged.
[-m] Mute. Log NOTHING. For running overnight, when you don't want a logfile to build up
[-v] Verbose. Log EVERYTHING.  See also globals.h for finer control.

User Interface:
 
'q' Quit program. 

' ' Pause and observe, userObserve()

WHILE OBSERVING    //TODO broken with the individual trials.  meaning you typically can't ever pause and observe. 

'q' Quit program.

'wasd' Moves the cursor around

' ' Unpause   

'.' Allows one turn to execute and then comes back to userObserve()

',' Step through one cycle of opcodes in currently selected agent

'ijkl'  "direct control" moving agent 0 around.

'p' logs a junk message. I'm pretty sure that's just for debugging



145th:
So this genocide automata demo thing from japan via PlayThisThing got me thinking. First off, I'm STILL not quite sure what the hell the damn program does. How do things die? what does territory owned do? He splits up the field into about a 20,20 grid which, along with some flocking algorithms, makes for a lot of the formations you see. But what the hell are the combat algorithms? 

Anyway, it set my mind on fire and I want to make it better. Get some GA and put it to work on troop AI for medival combat. Start with A SINGLE swordsmen hunting down peasents in a small room. This is to get it to learn how to go towards targets in a fully deterministic environment. Then let the peasents learn to flee. Add some friends. Then swordsmen fighting each other. Add walls, shields, archers, standards, buildings, etc etc etc.
    [2016 reboot: JESUS, why didn't I stick to that idea. Throwing everyone in a pit and hope they learn was a bad idea. Baby steps  -> progressivly harder trials]

Grid-based combat.  Agents hold a sword in front of them. It's actually a lot like "DROD", from 1997. 

Actions
move 1 space,  4 way.
Rotate sword/facing 90 deg
Input is just an offset from their current position: Forward, right/left.   (Although I'm not seeing them utalize it. Consider short-cut opcodes)



Mechanics:
One unit/item/object per tile. No overlap. 
Turn order is random. 
Strict orthaganal
collision detection is sequential by unit, wielded items first, then the unit itself.
Collision then resolved via a giant 2D array of object interaction.
  unit on unit collision: fail, and both miss their next turn (stunned).
  unit on sword: Death, unit is removed, so sucess/fail is moot
  sword on unit: success, and death of the target you stabbed
  sword on sword: chance, both owners stunned, one of them at random is pushed back.
  Other objects to add: Shield, Arrow, pikeshaft, horse, Compound objects.

Fitness:
 +1000 made a kill
 -1 if they spend too long thinking and get cut off.
 Rewards based on specific trials. 
    +1 Did you even move?
    +Inverse of distance to target
 reset every round


Trials:
Small room, like 10x10. 
-Does it move? If it doesn't move after 5000 cycles, it's not viable. 
-Can it tackle staticly placed immobile targets, all by itself?
-Can it find a randomly placed target?
Big room
-The original idea of throwing everyone into a room and an open death-match

If an agent can't pass a trial, then it does not advance and waste clockcycles.



========================= 2016 Reboot =========================
Oh sweet jesus. Picking up old projects.  What a nightmare.  Well at least I know I learned a lot between here and there. 

GOAL: Start them off with simple goals and build them up PER THE ORIGINAL IDEA. throwing them into a ring and hoping they learn from themselves makes a constantly moving goalpost.

1) Source control for the love of god. I even have comments about this.   Oh eww, I have random multiple folders.
  DONE. That's easy at least.   And some history is preserved.
2) Why the fuck did I use dynamic memory?   There's X agents with Y mind-space. It doesn't resize. At the first bug, chop it all and use arrays. Otherwise, live and let live. 
  DONE
3) OH GOD document your shit. WAY too much shit just thrown in as an experiment. Walk through code and doxygenize the header files
4) Quick doc the arguments and input controls. 
  DONE
5) Time is fucked between windows and linux.   Fix it, or better yet, standardize.
  hmmmm,    lastCycle is incrementing by 10,000.  usleep(1) is betraying us
  DONE


(Whelp, THAT was a fucking curveball. Makes me feel like any point in my life I'm free enough and together enough to actually work on this project, something is about to explode in my face)

========================= 2018 Reboot =========================
!) Everything else aside, you've got to make it demo-able for that presentation.  

1) Bugs first.   
  DONE-Missing sword.   You can hit it with a "wtf" comment. Hit in debugger, call tree. 
2) Well let's just say that was the end of the last plan... 


========================= 2021 ===============================
Kicking it around some more. 
+opcode stepping, which should help with debugging. 
-Move to 32-bit agents minds bigger than 256
-Organize trials a little better. Something other than recompiling.
-Documentation: The dependency graph was a good idea.
-Analysis: Gotta know what is and isn't working. 


