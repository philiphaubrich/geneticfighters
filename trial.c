#include "trial.h"
#include "logging.h"
#include "globals.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

struct Trial g_trials[NUM_TRIALS];


static int distance(int x1, int y1, int x2, int y2)
{
  return abs(x1-x2)+abs(y1-y2);
}


//Tied to SMALL_ROOM
int didYouEvenMove(agentIndex agentInTrial, TrialOutcome_t* outcome)
{
  if(g_mon[agentInTrial].x == 5 &&
     g_mon[agentInTrial].y == 5)
  {
    *outcome = TRIAL_OUTCOME_ABORT;
  }
  else
  {
    g_mon[agentInTrial].fitness += 1;
    logging(LOG_FITNESS, "%s gains %d by taking a step. %d,%d\n", g_mon[agentInTrial].name, 1, g_mon[agentInTrial].x, g_mon[agentInTrial].y);
    *outcome = TRIAL_OUTCOME_CONTINUE;
  }
  return 0;
}

//Esentially, I just want to reward them partial success if they get close to target
//  That gradual slope in fitness is REAL important
//  .... I'm not sure this is a real good idea. I mean, you kill targets, this gets you LESS points
int distToDummy(agentIndex agentInTrial, TrialOutcome_t* outcome)
{
  int x,y;

  for(x=0; x < SMALL_ROOM_SIZE; x++)
    for(y=0; y < SMALL_ROOM_SIZE; y++)
      if(g_map[x][y].tile == TILE_DUDE)
      {
        int dist = distance(x,y, g_mon[agentInTrial].x, g_mon[agentInTrial].y);
        if( dist > 0)
        {
          g_mon[agentInTrial].fitness += SMALL_ROOM_SIZE - dist;
          logging(LOG_FITNESS, "%s gains %d at %d,%d\n", g_mon[agentInTrial].name, SMALL_ROOM_SIZE-dist, g_mon[agentInTrial].x, g_mon[agentInTrial].y);
        }
      }
  return 0;
}

int nothingSpecial(agentIndex agentInTrial, TrialOutcome_t* outcome)
{
  return 0;
}


//---------------Public facing


//Generally going from easy to hard.
int initTrial()
{
  int trial = 0;

  snprintf(g_trials[trial].name, TRIAL_NAME_SIZE, "MoveItBaby");
  g_trials[trial].id = TRIAL_STATIC_PEASANTS;
  g_trials[trial].type = TRIAL_TYPE_SOLO;
  g_trials[trial].map = MAP_SMALLROOM;
  g_trials[trial].roundLength = 1;
  g_trials[trial].processingTime = 5000;
  g_trials[trial].numAgents = 1;  //More like size of group... yeah, rename it. TODO
  g_trials[trial].placement = PLACEMENT_CENTER;
  g_trials[trial].outcome = TRIAL_OUTCOME_ABORT;
  g_trials[trial].fitnessFuncPtr = &didYouEvenMove;
  trial++;

  snprintf(g_trials[trial].name, TRIAL_NAME_SIZE, "StaticPeasants");
  g_trials[trial].id = TRIAL_STATIC_PEASANTS;
  g_trials[trial].type = TRIAL_TYPE_SOLO;
  g_trials[trial].map = MAP_SMALLROOM;
  g_trials[trial].roundLength = 20;
  g_trials[trial].processingTime = 5000;
  g_trials[trial].numAgents = 1;
  g_trials[trial].placement = PLACEMENT_CENTER;
  g_trials[trial].fitnessFuncPtr = &didYouEvenMove;
  g_trials[trial].outcome = TRIAL_OUTCOME_CONTINUE;
  g_trials[trial].fitnessFuncPtr = &didYouEvenMove;   //Should be "time till all targets are gone"  reward for kills is still there.
  trial++;

  snprintf(g_trials[trial].name, TRIAL_NAME_SIZE, "RandPeasant");
  g_trials[trial].id = TRIAL_RAND_PEASANT;
  g_trials[trial].type = TRIAL_TYPE_SOLO;
  g_trials[trial].map = MAP_SMALLROOM;
  g_trials[trial].roundLength = 200;
  g_trials[trial].processingTime = 5000;
  g_trials[trial].numAgents = 1;
  g_trials[trial].placement = PLACEMENT_CENTER;
  g_trials[trial].outcome = TRIAL_OUTCOME_CONTINUE;
  g_trials[trial].fitnessFuncPtr = &distToDummy;
  trial++;

/* It ain't ready yet. Needs debugging
  snprintf(g_trials[trial].name, TRIAL_NAME_SIZE, "FleeingPeasant");
  g_trials[trial].id = TRIAL_FLEEING_PEASANT;
  g_trials[trial].type = TRIAL_TYPE_SOLO;
  g_trials[trial].map = MAP_SMALLROOM;
  g_trials[trial].roundLength = 50;
  g_trials[trial].processingTime = 5000;
  g_trials[trial].numAgents = 1;
  g_trials[trial].placement = PLACEMENT_CENTER;
  g_trials[trial].outcome = TRIAL_OUTCOME_CONTINUE;
  g_trials[trial].fitnessFuncPtr = &distToDummy;
  trial++;
*/

//------------ Solo trial before,  Group trials after ---------

  snprintf(g_trials[trial].name, TRIAL_NAME_SIZE, "DeathMatch");
  g_trials[trial].id = TRIAL_DEATHMATCH;
  g_trials[trial].type = TRIAL_TYPE_COMPETATIVE;
  g_trials[trial].map = MAP_WALL_PERIMITER;
  g_trials[trial].roundLength = g_roundLength;
  g_trials[trial].processingTime = g_processingLimit;
  g_trials[trial].numAgents = 10;
  g_trials[trial].placement = PLACEMENT_RAND;
  g_trials[trial].outcome = TRIAL_OUTCOME_CONTINUE;
  g_trials[trial].fitnessFuncPtr = &nothingSpecial;
  trial++;

  if(trial > NUM_TRIALS)
  {
    logging(LOG_ERR, "Whoa there, you're overflowing the trials\n");
    return -1;
  }
  return 0;
}


//Reset Agent(s) according to trial
void setupTrialAgent(agentIndex agent, int trial)
{
  //Placing agents
  int x;
  int y;
  switch(g_trials[trial].placement)    
  {
    case PLACEMENT_CENTER:   //hmmm, tied to small_room...
      x = SMALL_ROOM_SIZE/2;
      y = SMALL_ROOM_SIZE/2;
      putAgentSpecificSpot( agent, x,y, NORTH);
      break;
    case PLACEMENT_RAND:
      if(putAgentRandSpot(agent))
      {
        logging(LOG_ERR, "ERR placing random agent\n");
      }
      break;
    case PLACEMENT_SPECIFIC:
      //TODO
      break;
    case PLACEMENT_CORNERS:
      //TODO
      break;
    case PLACEMENT_WESTSIDE:
      //TODO
      break;
    default:
      logging(LOG_ERR, "wut?\n");
  }

  if( g_trials[trial].type != TRIAL_TYPE_SOLO) //Group trial
    g_mon[agent].isDoneWithSoloTrials = 1;  //ok, the convention is solo trials before group trials. Solo trials are only done once. 

  //not fitness or cycles, which persist through the round.... ooooooh,   so the END of ALL trials need to reset fitness
  g_mon[agent].active = 1;
  //Aaaaaand probably the PC reg. I mean, so they can... init and stuff. If you yank their position they should probably reset
  g_mon[agent].mind[ADDRESS_PC] = CODE_START;

}

void setupTrialMap(int trial, int* trialLength)
{
  generateMap(g_trials[trial].map);

  //...I dunno, I might be overthinking the whole struct thing.
  //    Trials are probably going to be special snowflakes.   
  switch(g_trials[trial].id)
  {
    case TRIAL_STATIC_PEASANTS:
      g_map[SMALL_ROOM_SIZE/2][SMALL_ROOM_SIZE/2 - 3].tile = TILE_DUDE;
      break;
    case TRIAL_RAND_PEASANT:
      g_map[rand()%(SMALL_ROOM_SIZE-2)+1][rand()%(SMALL_ROOM_SIZE-2)+1].tile = TILE_DUDE;
      break;
    case TRIAL_FLEEING_PEASANT:
      //TODO. Small room. But need a method of making something other than GA minds operate in the game space
      break;
    case TRIAL_DEATHMATCH:
      //The default I started with.  But... I really don't have anything here?
      break;
  }

  *trialLength = g_trials[trial].roundLength;
}



//Careful with this one, it's not guarenteed to hit all agents for each trial. 
void endTrial(agentIndex agent, int trial)
{
  if(g_trials[trial].type != TRIAL_TYPE_SOLO)
  {
    //This function is just for solos
    return;
  }
  TrialOutcome_t outcome;
  if( *g_trials[trial].fitnessFuncPtr != NULL)
  {
    (*g_trials[trial].fitnessFuncPtr)(agent, &outcome);
    if(g_trials[trial].outcome == TRIAL_OUTCOME_ABORT && outcome == TRIAL_OUTCOME_ABORT)   //Hmmm, this is an awkward double-usage of an enum. IT's the type of trial AND the return value of the trials fitness function. But... they're kinda the same thing. 
    {
      // ok, it failed a trial that deems the agent non-viable.  more than just LOGGING it, we want to not waste time with it in the big deathmatch 
      //    ...We remove it, nuke the agent slot, and leave it as a hole to fill at the end of the round. 
      //         It means our list of shuffled agents has to ignore empty slots.   
      //         Also means we could get to the death-match with 0 viable agents.   Can't trust g_numAgents anymore. 
      logging(LOG_MIDGAME, "Agent %s,%d is judged non-viable in trial %d, %s. \n",
                           g_mon[agent].name, 
                           agent, trial,
                           g_trials[trial].name);
      g_mon[agent].occupied = 0;
    }
  }
  g_mon[agent].active = 0;


}




