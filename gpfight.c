/*
Beginings of GP fighters

Open source, GPLv3
Signed in blood, -Philip Haubrich, June 12th 2012

So a third of this project is doing awesome stuff with machine learning. 
Another third is me fussing over how I code things.
The final third is me pissing away good engineering practices because it's a home project and I'll code how I damn well feel like and it's nice to cut loose now and then.
  It's an odd mix.  

FYI, the acedamia keywords for this would be:
A multi-agent competative co-evolutionary interpreted linear genetic programming application using assembly instructions as the basic building block in a virtual environment.

multi-agent - As in, they can interact. Of course there's a populous, but unless they interact in the test phase, they work alone.
competative - And they work against each other
co-evolution - I wanted there to be preditors and prey (But that's a TODO)
interpreted - It's not really assembly. It's using an interpreted language. 
linear - As in an imperative programming. ie, "normal programming" as opposed to that learning tool tree-based stuff.
genetic programming - The output is code. As opposed to the broader "genetic algorithms" which can have any output. 
using assembly instructions as the basic building block   - My "big idea" claim to fame. Yeah, it's been done. Way before I tried. 1994. To the best of my knowledge there's not a catchy name for it. 
in a virtual environment. -Duh

  
TODO list:
=======
-Bug: Game crasher. Someone lost a sword, but it's probably a overrun issue with the 16-bit change. 
  Valgrind notices some stuff, but the output is weird. TODO

-Likely BUG: Somethign is up with displaying or executing their DNA in rounds after 1. 
  So... I set it up so they retain their PC from the previous round.  If I reset their position, I should likely reset thier head as well.  So.... Change that. 
  No... even then, they're WAY more active on round 0 than round 1. 
  Well, this makes it pretty fucking obvious that I need a pause and step control.   Step PC  and step Agent and  Step Trial/round 
-Likely Bug: There are agent names like j.agent and v.agent. It should be at least 3 with more added upon collision. wtf is this?
-We could decouple some of those dependencies more.   Get a graphvis plot of the dependencies. 
-Logging an error event should spam the log file with the current state. Everything. 
-A black-box of the previous state updated every tick-update. (so, when you log an error you can see what changed.)
-Ugh, need a way to skip to the next agent.   I'd like to see the agents do their thing with -s, but GET ON WITH IT!  
-Need to hammer out what the hell it takes to actually run and/or compile this thing.   Document it. 
  Linux: gcc, ncurses... uh?
  Windows: Mingw, msys, pdcurses.dll, pdcurses.lib?   Yep, that's prettymuch it for building it. Just put the .lib in your mingw/lib and keep the .dll in the local folder
  Windows, just to run: hmmm
-Profile it. See where we're spending most of our time crunching. File IO probably?
    -gprof. Use those skills!
-Multi-thread it. Multiple threads crunching minds saving their actions, once everyone has thought about what to do, do it.
  -Also makes actions more simultanious rather than the sequential way we have now, which is a perk. 
-More/better in-game control for looking about.
  -Set speed rate. 
  -Help dialog. And a prompt that 'h' exists.
  -Ability to save, breed, clone or something for hand-picked agents. I want to be able to see something cool, and save it. 
-Give users ability to speed up and slow down time.   -s and -f aren't cutting it
-Want to look at the log mid-game.  open, flush, and close f_log each round? 
-Could save some time (or make it less boring to watch) by having a watchdog kill the round if nobody has moved in the last 20 turns.
-MORE UNIT TESTING!!! BLARG!


IDEAS:
-EXPERIMENT: The more I look at this the more I think we need to vary the population size. Like there are lean years to cut out the fat and good years to breed diversity.  ...Frankly, this could be handled in a script pretty easily. Something that halted for errors, malformed agents or files would preserve a reasonably sized log file.  Gonna have to deal with population sizes shrinking.   If there are more agent files than we load.... then what?   Currently, if we run -a 100 and numAgents is bigger than 10, the death-match trial takes them in groups of 10.   Run it with 11... and the remainder never get run. It only runs full groups.   pft, I know that wasn't planned. 
    
-INSIGHT: Keep a metric of what addresses are used in a mind. If they don't even touch half the address space, the mind is perfectly big enough. There will definately come a point where we should bump MIND_SIZE up to 16-bit
    
DONE -Keep a metric of what opcodes are used. 
  
-Keep a metric of what addresses are used in a mind. If they don't even touch half the address space, the mind is perfectly big enough. There will definately come a point where we should bump MIND_SIZE up to 16-bit

DONEish -Different sort of trials:
  (So I can show off various things that this system can learn)
  -Does it even take a single step in 1 long processing cycle?   (Blocker, this isn't a viable baby. Don't bother with the other trials. Abort)
  D-Solo, Small room, static positioning of some peasants. Go shank'em
  D-Solo, small room, randomly placed peasant.
  -Open field, Move towards the sword in the corner. DON'T impale yourself. 
  -Static field of random obstacles. Get across the map. Basic pathfinding.
  -Constantly changing field of obstacles. Get across the map. Basic pathfinding.
  -Move the sword each round. requies them to cover ground looking for the sword rather than bumble about. 
  -Adding walls, move towards sword
  -set up a real maze, pathfinding to sword. (! hard heuristic. ... gotta reward them on... ground covered? Final distance?)
  -Random maze, pathfinding to sword.  They'd need a real algorithm. 
  -one sword, first to GRAB is victorious    <-a challenge with only one winner. hmmm  (Also you got rid of "grabbing" at some point)
  -one sword, # of kills
  D-CLASSIC: All initially equipped with swords,  # of kills

-EXPERIMENT: Re-arrange mind so that the 3-byte structure lines up with registers. 
So... start the code @ 10?
  Or I could restructure it so the virt machine can't set the PC to an argument.
  Or I could restrict writes to anything other than the 10 registers

+...there's something about agents named con that invokes the fucked up heap issue.
Yep, that is the weirded quirk I've ever seen. It makes an agent named "con" and somehow it saves the agent to stdout instead of the file.
Huh, this is a windows thing from when they didn't want you to confuse the stdout, I mean CONsole. So they don't let you name files/folders to "con".... but this is "con.agent"... hmmmm. Well, anyway, fuck you Windows.


-EXPERIEMENT: A cold storage for historical figures. Like, every 100,000 rounds or so save the top agent off to a seperate folder.
-A cold storage for historical figures. Like, every 100,000 rounds or so save the top agent off to a seperate folder.
Every 10,000 generations, load a random historical figure.
Save the top killer. So there's a global kill count, everytime someone breaks the record, save them as a historical figure.  abc_1_kill.agent

-INSIGHT: Generational data. So the parents hand off data to the kids. 
I was specifically thinking about keeping track of what type of mutation scheme they used. If we see a trend to or away from a type of mutation we'll know what worked.

-EXPERIMENT: Forcefully make sure agents have at least one opcode. They're pointless without it, why waste the cycles?

-EXPERIMENT: Population pools. Like, I can get WAY different outcomes with different runs. If I let two groups evolve seperately and then bring them together it's like this whole.... meta-level of evolution.  Honestly, the entire idea of keeping and re-introducing historical figures does this... through time. 

-The original implementation had agents picking up items like swords
-The original idea gave them more complicated toos like shields, spears, charging horses, rifles. 
-The original idea put them on opposing sides and had an "army" layer and team communication. 
-The original idea had co-evolution with the peasants learning to flee. 


LATER:
-Need to bump to 16 or 32-bit agents eventually. Probably. 
-Should have some way to split it into teams. Give each their starting area.
-Bring back holding objects.  GRAB/RELEASE opcodes again. 
-Rotating held objects need to pass through squares between pt A and pt B.
-Hmmmmmm rather than that movement all happening at once, the rotate command could move the held object one square towards rotation. 
-Add Spears,  compound object, a sword on the end of a shaft. (HA! We took a step BACK from this)
-Add compound objects like 1x3 shields
...sigh, that idea to make swords and shields agents that build a linked list to form more complex structure was a bad idea... Swords aren't agents.


in round 46,739 of 100,000 def, the champion for 1672 rounds, was cornered by was and zoq, and perished in last place. 


*/

#ifdef WINOWS_ENVIRONMENT
#include "curses.h"  //pdcurses for windows
#include <sys/types.h>
#else
#include <ncurses.h>  //ncurses for linux
#include <dirent.h>   //mucking about with directories
#endif

#include "gpfight.h"
#include "map.h"
#include "mon.h"
#include "gplib.h"
#include "gpopcode.h"
#include "aiAlgo.h"
#include "display.h"
#include "logging.h"
#include "trial.h"
#include "globals.h"
#include "minUnit.h"

#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
//#include <math.h>




//TODO take another pass and see if you can get rid of the globals. 

int g_round = 0;  
int g_topFit=0;
int g_numRounds = NUM_ROUNDS_DEFAULT;            ///< arg -r  Number of rounds to execute before quitting. Selection happens each round
int g_roundLength  = ROUND_LENGTH_DEFAULT;      ///< -l
int g_processingLimit = PROCESS_LIMIT_DEFAULT;  ///< arg -p
int g_consiousnessStream = 0;  // -c
int g_trialsForAgent = AGENT_TRIALS_DEFAULT;

int g_step = 0; // Step to the next turn (while paused)
int g_stepConsciousness = 0; // step to the next cycle / opcode (while paused and looking at an agent)
clock_t g_speed = CLOCKS_PER_SEC/1000; // Set with -d noDisplay  -f fastest (no delay)



//Logging
FILE* f_chart;




void initRound()
{
  logging(LOG_HIGHGAME, "\nRound: %d\n",g_round);

  // EXPERIMENT: randomize round length. 
  // g_roundLength = 100+(rand()%400); //Here's a fun experiement. 
  //    Wasn't really a big deal. 

  //ok, so Rounds no longer set up the map, each trial sets up a map. 
  //generateMap(MAP_SMALLROOM_STATICPEASANT);
  //generateMap(MAP_WALL_PERIMITER);

  //Sure, but some things need to be done at the start of each round regardless. 
  //   ... unless I'm trying to make individual trials keep their score. Which, I'm just not there yet. That's a TODO
  agentResetRound();

  return;
}




//On "books suck, get a nook". Anything worth reading should be on the internet. Anything worth reading later should be printed in a book. -4/12/12





//Decide who stays and who goes.
void endRound(int* lastBest, int* avg)
{
  struct FitnessMetric fit;

  fit.avgFitness = 0;
  fit.maxFitness = 0;
  fit.minFitness = 10000;
  fit.avgCycles = 0;
  fit.maxCycles = 0;
  fit.minCycles = 100000;
  fit.avgRounds = 0;
  fit.maxRounds = 0;
  fit.bredTop = 0;
  fit.medianFitness = 0;
  //fit.moves = 0;

  evaluatePopulationFitness(&fit);

  //We could gather more metrics about the whole gang....

  // Another notch on the wall.
  for(agentIndex i = 0; i<g_numAgents; i++)
  {
    g_mon[i].rounds++;
  }

  if(g_numAgents > 2) //Otherwise you're testing or something
    selection(&fit);
  else
    logging(LOG_VERBOSE, "Oye, you skip selection when the pools really small\n");

  chartlog(&fit);

  *lastBest = fit.maxFitness;
  *avg = fit.avgFitness;
  return;
}



//Do whatever it is the thing is going to do.
void advanceTime(int* time, int agentList[], int listSize)
{
  //Cycle through all agents, process mind, perform action
  agentIndex agent;
  direction_types direction;

  (*time)++;

  for(int listIndex=0; listIndex<listSize; listIndex++)
  {
    agent = agentList[listIndex];

    if(g_mon[agent].active == 0)  //Dead men take no time
      continue;

    switch(g_mon[agent].aiAlgo)
    {
      case AIALGO_GENETICPROG:
        direction = mindCrunch(agent);
        break;
      case AIALGO_FLEENEAR:
        direction = aiFlee(agent);
        break;
      default:
        logging( LOG_ERR, "wtf, you have an agent with a wayward AI algo\n");
        return;
    }


    if(agentMove(agent, direction))
    {
      //logging(LOG_VERBOSE,"%s @ %d,%d, moves %d\n",g_mon[agent].name, x,y, direction);

      // return whether the move action is successful or not. Feedback helps a lot.
      // NOTE: this fucks up all sorts of shit if agentMove frees memory.   
      g_mon[agent].mind[1] = 1;    
    }
    else
    {
      //logging(LOG_VERBOSE,"%s @ %d,%d, fails to move %d\n",g_mon[agent].name, x,y, direction);
      g_mon[agent].mind[1] = 0;
    }
  }
}





void doit()
{
  int userInput;
  int wait=0;
  clock_t lastCycle =0;
  int time =0;
  int lastBestFit=0;
  int avgFit=0;

  //TODO: why Key_b2?
  userInput = KEY_B2;

  //Load/generate agents
  initMon();

  while(g_round < g_numRounds) 
  {    
    initRound();
    int trial;
    for(trial =0; trial < NUM_TRIALS; trial++)
    {
      logging(LOG_VERBOSE ,"~~Trial %d~~\n", trial);

      //Rounds are now... a set of trials. Individual and a group deathmatch
      // Get a shuffled list of viable agents.  Early trials can weed out unviable agents 
      int shuffledListOfAgents[MAX_NUM_AGENTS];  
      int numViableAgents;
      shuffleMonList( shuffledListOfAgents, g_numAgents, &numViableAgents ); 
      logging(LOG_VERBOSE ,"%d still viable\n", numViableAgents);
      //Yeah, note that last bit means we could be working with 0 to g_num agents after this.

      //For all agents in the populous, chunk them into groups and put them through the trial. 
      for( int allAgents = 0; allAgents < numViableAgents; allAgents += g_trials[trial].numAgents)
      {
      
        // if it's a solo trial, and they've already done solo trials, continue. 
        if(g_trials[trial].type == TRIAL_TYPE_SOLO && 
            g_mon[shuffledListOfAgents[allAgents]].isDoneWithSoloTrials) continue;

        int trialLength;
        setupTrialMap(trial, &trialLength);

        //For each/all agent(s) in trial
        logging(LOG_VERBOSE ,"~~Group: ");
        for( int i = allAgents;  i < allAgents+g_trials[trial].numAgents && i < numViableAgents; i++)
        {
          logging(LOG_VERBOSE ," %s, ", g_mon[shuffledListOfAgents[i]].name);
          setupTrialAgent(shuffledListOfAgents[i], trial);
        }
        logging(LOG_VERBOSE ,"\n");


        time = 0;
        while(time < trialLength)  //Visible Loop
        {
          paintMap();
          mvprintw(0,0,"Round: %5d, %s Trial: %5d Time: %4d TopFit: %5d", g_round, g_mon[shuffledListOfAgents[allAgents]].name, trial, time, g_topFit);

          //logging(LOG_CONSIOUSNESS ,"Turn: %d\n", time);  //Too fucking messy in the log

          //Yeah, this part where there's multiple sections for UI sucks. OVERHAUL, IT'S TIME!
          userInput = getch();  //Needs overhaul later, when we allow user to pause mid-game
          //Whoa, for some reason it fails to paint anything unless we getch()

          if(userInput == '<')
            g_speed = g_speed /2 + 1 ;
          if(userInput == '>')
            g_speed = g_speed * 2 ;
          if(userInput == 'q')
            quitProgram();
          if(userInput == ' ') 
            g_step = 1;
          if(g_step == 1)
            userObserve();

          //Deal with remainder groups. 
          int groupSize;
          if(allAgents +  g_trials[trial].numAgents > numViableAgents)
          {
            groupSize = numViableAgents - allAgents;
          }
          else
          {
            groupSize = g_trials[trial].numAgents;
          }


          advanceTime(&time, &(shuffledListOfAgents[allAgents]), groupSize);

          //Regulate time      
          if(g_speed != NO_DISPLAY)
          {
            while(  clock() - lastCycle < (clock_t)g_speed)
            {
              //usleep(1);
              //fuckit, spinlock
            }

            lastCycle = clock();
          }
        }
        endTrial( shuffledListOfAgents[allAgents], trial);  //Only has an effect for solo trials, so that's the guy
      }
    }
    g_round++;

    //The dark display, a progress bar essentially.
    if(g_speed == NO_DISPLAY && g_numRounds > 10)
      if(g_round % (g_numRounds/10) == 0)
        printf("%d topFit: %d\n",wait++, g_topFit);


    endRound(&lastBestFit, &avgFit);
   
    // An ok experiment, but I don't know if I'll want it again 
    // break once we hit a target fitness
    //if(lastBestFit >= MAX_POSSIBLE_FITNESS * g_trialsForAgent && g_trialsForAgent != 0)  
    //  break;
  }


  printf("lastBest: %d Avg: %d \n", lastBestFit, avgFit);
}




/** Handles user input while in userObserve()
 *  'q' Quit program.
 *
 *  'wasd' Moves the cursor around
 *
 *  ' ' Unpause
 *
 *  '.' Allows one turn to execute and then comes back to userObserve()
 *
 *  ',' Allows one cycle to execute and then comes back to userObserve()
 *
 *  'ijkl'  "direct control" moving agent 0 around.
 *
 *  'p' logs a junk message. I'm pretty sure that's just for debugging
 *
 */
char applyInput_CursorControl(int in, int* x, int* y)
{
  switch(in)
  {
    case 'w':
      *y = *y-1;
      break;
    case 'a':
      *x = *x-1;
      break;
    case 'd':
      *x = *x+1;
      break;    
    case 's':
      *y = *y+1;
      break;

    case 'Q':
    case 'q':
      quitProgram();
      break;
    case ' ':   //unpause
      g_step = 0;
      return 0;  //Escape userObserve() loop
    case '.':
      g_stepConsciousness = 0;
      g_step = 1;
      return 0;
    case ',': //next cycle
      g_stepConsciousness = 1;
      return 0;
      break;
    //case 'n': //Step to the next Agent  TODO
    //case 'N':  //Step to the next Round (or trial)  TODO
    case 'p':   ///What was this for again?
      logging(LOG_ERR ,"neveryoumind, Imma break this shit yo\n");
      break;
    case 'v': //Cause other letters were taken
      //saveMap();  //TODO, Not sure what the use-case was anymore though
      break;
    default:
      mvprintw(0,0,"~~bad key~~");
      break;
  }
  return 1;
}

//Mid battle Users's-lookin-at-shit mode
//  It actually has it's own big loop, paint, and user-interface.   Huh.
//  I'm not real sure where this goes
void userObserve()
{
  int userInput;
  clock_t lastCycle = 0;
  static int cursorX = MAP_SIZE_X/2;
  static int cursorY = MAP_SIZE_Y/2;
  int blinkTime = 0;

  agentIndex agent;

  while(1) 
  {
    //Set cursorAgent  equal to closest agent to cursor
    agent = nearestAgent(cursorX, cursorY, 1);   //wtf was the idea behind excluding the one AT the cursor?

    //update map
    paintMap();

    //Paint cursor
    //MY GOD.... BLINKIN
    if(blinkTime == 1)
    {
      blinkTime = 0;
      if(agent  != NO_AGENT)
      {
        mvprintw( g_mon[agent].y + 1, g_mon[agent].x, "_");
      }
    }
    else
    {
      mvprintw(cursorY,cursorX,"_");
      blinkTime = 1;
    }
    //FUCK YOU  BLINKIN BASTARD!

    
    // display their PC, OPCODES, (and registers?)   
    if( agent  != NO_AGENT)
    {
      mvprintw(1,40,"                ");
      mvprintw(2,40,"%s x:%d y:%d facing: %d fit:%d",g_mon[agent].name, g_mon[agent].x, g_mon[agent].y, g_mon[agent].facing, g_mon[agent].fitness);
      displayMind(agent, mvprintw);
    }
    else
    {
      mvprintw(1,40,"no agent selected");
    }

    //Get Input
    userInput = getch();
    if( !applyInput_CursorControl(userInput, &cursorX, &cursorY))
      return;

    //Regulate time
    while(  clock() - lastCycle < QUARTERSEC/4 )
    {
      //usleep(1); fuckit, spinlock
    }
    lastCycle = clock();
  }
}


