#ifndef __OPCODES__
#define __OPCODES__
/*  opcodes
This is what GPlib started out as, but eventually took on enough fitness
 and management that the opcodes were a side-feature

*/

#include "globals.h"
#include "gpfight.h"

#define OPCODENAME_MAXSIZE 30

//Foward declaration... hmmm     todo... what?      God this code-base is an embarrasment 12 years later...
struct Agent;




/* The original idea. Yeah, that didn't happen.
struct DNA
{
    char op;
  char a;
    char b;
};
*/

typedef enum
{
  OP_NOP,   // 0
  OP_MOV,   // 1
  /*
    arg1 = arg2.  MOV 4,5 will place the value at memory[5] into memory[4]
    ARG1IMM:  MOV 4,5 sets memory[4] equal to 5
    ARG2IMM:  MOV 4,5 will use memory[4] as a pointer and set a value
  */
  OP_ADD,   // 2
  OP_SUB,   // 3
  OP_MULT,  // 4
  OP_DIV,    // 5
  OP_MOD,    // 6
  
  OP_CMP,   // 7  TODO: Add me back
  OP_JEQL,  // 8
  OP_JNEQL,  // 9
  OP_JLESS,  // 10
  OP_JMORE,  // 11
  //OP_JMP,    // 12   // Jump is redundant with the ability to set the PC counter with MOV [0]    TODO: So? It's easier. add it back
  
  OP_LOGAND,    // 12
  OP_LOGOR,      // 13
  
  OP_BITAND,    // 14
  OP_BITOR,      // 15
  OP_BITXOR,    // 16
  OP_BITNOT,    // 17
  
  OP_IN_LOOK,    // 18  look tile, deltaX, deltaY.  Load into AX
  OP_OUT_MOVE,  // 19
    /*  ENDS TURN!
      Move based of of AX, interpreted as a direction.    //TODO what? oh god, give them alternatives so they can pass in an argument
      rest (or any other non-direction) = no movement, end of turn      
      Right now the arguments are ignored, but we could easily make arg1 the direction  OP_OUT IMM 1 X would be the same thing    //TODO yeah, this. No need to bind them
      Sets AX with success or fail. 
    */
  OP_IN_RAND,    // 20 Load AX with a random value   //TODO experiment with not doubleing up AX so much. 
  OP_IN_FACING,  // 21 Load AX with agent->facing. Could be useful? Like a compass?
          //Unsupported past this.  NUMOPCODES is 21
          //
          //Yeah, these are probably bad ideas down below. TODO axe
  OP_IN_HOLDING,  // 22 load AX with the tile of what we're holding. Blank for nothing.
  OP_OUT_GRAB,  // 23 ENDS TURN!   Tries to grab what's in front of us.  Loads AX with success or fail.

  OP_OUT_SAY,    // 24  Transmit arg1 [over channel arg2]  to....   something?  a single global storage? Writting directly to other agent's brains?
  OP_IN_LISTEN,  // 25  I have no idea.
  MAX_OP      //NOT AN OP, KEEP AT END
}op_codes;

//Think about:   Input: my facing    Input: what am I holding?    output: Grab

/*Rant
Yeah, something like a factory pattern would keep this a lot more orderly, but adding an opcode really  isn't all that bad. I mean, there are... what, 5 places you need to dink with. But if you just search the project for one of the opcodes, those 5 places are RIGHT THERE. Maybe this structure just isn't complex enough to warrent it.
*/


uint16 mindCrunch(agentIndex agent);

void getOpCodeName(op_codes op, char* name, int nameSizeInBytes);
op_codes getOpCodeNumber(char* name);
//void displayMind(agentIndex agent);
void displayMind(agentIndex agent, int(*dispFunc)(int, int, const char*, ...) );

#endif

