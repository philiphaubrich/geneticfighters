///@file main.c It holds main(), handles command line arguments, kicks off init and cleanup.
//  It also handles the two big loops with: User input, doing stuff, painting screen, regulating fps.
//  The first goes through GP rounds and generally does the program
//  The second is for player observations, and handles user control and feedback and painting the screen


#ifdef WINOWS_ENVIRONMENT
#include "curses.h"  //pdcurses for windows
#include <sys/types.h>
#else
#include <ncurses.h>  //ncurses for linux
#include <dirent.h>   //mucking about with directories
#endif

#include "gpfight.h"
#include "gplib.h"
#include "gpopcode.h"
#include "logging.h"
#include "trial.h"
#include "display.h"
#include "globals.h"
#include "minUnit.h"

#include <stdlib.h>
#include <time.h>
#include <getopt.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
//#include <math.h>





int g_seed;
int g_test = 0; //-t


char helpStr[] ={"Genetic Fighters. \n\
A multi-agent competative co-evolution linear genetic programming application using assembly instructions as the basic building block in a virtual environment.\n\
By Philip Haubrich \n\
[-a numAgents]  How many agents to generate. Default: 15\n\
[-r numRounds]  How many rounds to process. Program will quit afterwards. Default: 10   0 for infinate\n\
[-l roundLength] Number of turns in a round. (nontrials) Default: 100\n\
[-i number]  Number of time agents should run a trial, 0 for every round\n\
[-p maxCycles] How many instructions to let them crunch before forcing an end of round.\n\
[-f] Run fast. Limited only by your processor. Default limits turns to 1 millisecond\n\
[-s] Run slow. Turn is limited to .25 seconds. Default limits turns to 1 millisecond\n\
[-d] Run dark. No input or output. Runs really fast though.  \n\
[-c] Log consiousness stream. Every opcode is logged.\n\
[-m] Mute. Log NOTHING. For running overnight, when you don't want a logfile to build up\n\
[-v] Verbose. Log EVERYTHING.  See also globals.h for finer control.\n\
[-t] Test mode. Let's see if I kept up unit-testing\n\n\
UI: \n\
 'q' Quite program\n"};
void processArguments(int argc, char** argv)
{
  int c;  //Damn you, you terse C motherfuckers and your shitty terse examples

  opterr = 0;

  while ((c = getopt (argc, argv, "ha:r:l:p:i:fdscmvt")) != -1)
  {
    switch (c)
    {
      case 'h': //Help
        fprintf(stderr, "%s", helpStr);
        exit(0);
      case 'a':  //Agents
        sscanf(optarg, "%d", &g_numAgents);
        if(g_numAgents > MAX_NUM_AGENTS)
        {
          logging(LOG_ERR, "WARN: max agents is %d\nSorry, it's super lame with fixed-arrays. (But it's so much easier to debug!)\n", MAX_NUM_AGENTS);
          g_numAgents = MAX_NUM_AGENTS;
        }
        break;
      case 'r':  //Num Rounds
        sscanf(optarg, "%d", &g_numRounds);
        if(g_numRounds == 0)
          g_numRounds = 2147483647;
        break;
      case 'l': //round Length
        sscanf(optarg, "%d", &g_roundLength);
        break;
      case 'p':  //maxCycles
        sscanf(optarg, "%d", &g_processingLimit);
        break;
      case 'i':   //Number of trials
        sscanf(optarg, "%d", &g_trialsForAgent);  //TODO
        break;
      case 's':  //slow
        g_speed = QUARTERSEC;
        break;
      case 'f':  //fast
        g_speed = FASTEST;
        break;
      case 'd':  //dark, no display  (superfast)
        g_speed = NO_DISPLAY;
        g_step = 0; //make sure we don't wait for IO that's never coming.
        break;
      case 'c':
        g_consiousnessStream = 1;
        g_logging |= LOG_CONSIOUSNESS;
        break;
      case 'm':
        g_logging = 0; //clears all logging options
        break;
      case 'v':
        g_logging = LOG_ALWAYS; //verbose, log EVERYTHING
        break;
      case 't':
        g_test = 1;
        break;
      case '?':
        if (optopt == 'c')
        {
          fprintf(stderr, "Option -%c requires an argument.\n", optopt);
        }
        /*else if (isprint (optopt))
          {
          fprintf (stderr, "Unknown option `-%c'.\n", optopt);
          }*/
        else
        {
          fprintf (stderr,"Unknown option character `\\x%x'.\n", optopt);
          fprintf (stderr,"Try -h instead\n");
        }
        exit(0);
      default: 
        return;
    }
  }
}

void init()
{
  if(g_speed != NO_DISPLAY)
  {
    initscr();
    resize_term(SCREEN_SIZE_Y, SCREEN_SIZE_X); 
    //WINDOW *resize_window(WINDOW *, int, int);
    getmaxyx(stdscr, LINES, COLS);
    raw();
    cbreak();
    noecho();
    nodelay(stdscr, TRUE); //getch() doesn't halt the program.  
    keypad(stdscr, TRUE);
    curs_set(0);
  }
  g_seed = abs(time(NULL));
  srand(g_seed);

  logInit();
  initTrial();
  f_chart = fopen("chart.txt", "wa");  //TODO move this too
  logging(LOG_ALWAYS ,"\n%d\n",(int)time(NULL));
  return;
}



void quitProgram()
{
  resize_term(30, 80);
  logClose();
  fflush(f_chart);
  fclose(f_chart);
  if(g_speed != NO_DISPLAY)
    endwin();
  exit(0);
}



int main(int argc, char** argv)
{
  
  processArguments(argc, argv);

  //uhhhhh, hook for unit testing?
  if(g_test == 1)
  {
    //test();
    exit(0);
  }

  init();
  doit();
  quitProgram();
  return 0;
}
