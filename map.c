
#include "logging.h"
#include "mon.h"
#include "map.h"
#include "globals.h"

#include <stdlib.h>

struct map g_map[MAP_SIZE_X][MAP_SIZE_Y];       ///< The play area. Hold agents, terrain, and positions.


// Check if a point is in or out of the map,
// Return 1 if good, 0 if bad
char inMap(int x, int y)
{
  if( x < 0 || y < 0 || x >= MAP_SIZE_X || y >= MAP_SIZE_Y)
  {
    return 0;
  }
  return 1;
}

tile_types getTileType(int x, int y)
{
  if(!inMap(x,y))
    return TILE_OOB;
  else 
    return g_map[x][y].tile;
}

//Clears map, set up walls or whatever
void generateMap(map_types mapType)
{
  int x,y;

  //Clear map?
  for(x=0; x < MAP_SIZE_X; x++)
  {
    for(y=0; y < MAP_SIZE_Y; y++)
    {
      g_map[x][y].agent = NO_AGENT;
      switch(mapType)
      {
        default:
        case MAP_EMPTY: 
          g_map[x][y].tile = TILE_BLANK;
          break;
        case MAP_RANDOM_WALLS:
        case MAP_RANDOM_SWORDS:
          switch(rand()%5)
          {
            case 0:
              g_map[x][y].tile = (mapType==MAP_RANDOM_WALLS ? TILE_WALL : TILE_SWORD);
              break;
            default:
              g_map[x][y].tile = TILE_BLANK;    
          }
          break;
      }
    }
  }

  if(mapType == MAP_SWORD_PERIMITER)
  {
    //Line the place with swords
    for(x=0; x < MAP_SIZE_X; x++)
      g_map[x][0].tile = TILE_SWORD;
    for(x=0; x < MAP_SIZE_X; x++)
      g_map[x][MAP_SIZE_Y-1].tile = TILE_SWORD;
    for(y=0; y < MAP_SIZE_Y; y++)
      g_map[0][y].tile = TILE_SWORD;
    for(y=0; y < MAP_SIZE_Y; y++)
      g_map[MAP_SIZE_X-1][y].tile = TILE_SWORD;
  }
  if(mapType == MAP_WALL_PERIMITER)
  {
    //Line the place with swords
    for(x=0; x < MAP_SIZE_X; x++)
      g_map[x][0].tile = TILE_WALL;
    for(x=0; x < MAP_SIZE_X; x++)
      g_map[x][MAP_SIZE_Y-1].tile = TILE_WALL;
    for(y=0; y < MAP_SIZE_Y; y++)
      
      g_map[0][y].tile = TILE_WALL;
    for(y=0; y < MAP_SIZE_Y; y++)
      g_map[MAP_SIZE_X-1][y].tile = TILE_WALL;
  }

  if(mapType == MAP_SMALLROOM)
  {
    //Line the place with walls
    for(x=0; x < SMALL_ROOM_SIZE; x++)
      g_map[x][0].tile = TILE_WALL;
    for(x=0; x < SMALL_ROOM_SIZE; x++)
      g_map[x][SMALL_ROOM_SIZE-1].tile = TILE_WALL;
    for(y=0; y < SMALL_ROOM_SIZE; y++)
      g_map[0][y].tile = TILE_WALL;
    for(y=0; y < SMALL_ROOM_SIZE; y++)
      g_map[SMALL_ROOM_SIZE-1][y].tile = TILE_WALL;
  }

}



int tileLookup(tile_types tile)
{
  switch(tile)
  {
    case TILE_OOB:     return 0;
    case TILE_BLANK:   return 1;
    case TILE_WALL:    return 2;
    case TILE_DUDE:    return 3;
    case TILE_SWORD:   return 4;
    case TILE_SHIELD:  return 5;
    default: 
                       logging(LOG_ERR ,"ERR, tile lookup: %d\n", tile);
                       return -1;
  }
}



// Also, it's not entirely random, it favors the middle
char putAgentRandSpot(agentIndex agent)
{
  int x, y, facing, ret;
  int timeout=10000;

  if(agent == NO_AGENT || agent > g_numAgents)
  {
    logging( LOG_ERR, "ERR, trying to put a non-agent somewhere: %d\n", agent);
    return -1;
  }

  do
  {
    //IE, in the middle-half
    x = (rand() % (MAP_SIZE_X/2))+(MAP_SIZE_X/4);
    y = (rand() % (MAP_SIZE_Y/2))+(MAP_SIZE_Y/4);
    facing = rand() % 4;

    if(!timeout--)
    {
      logging(LOG_ERR, "ERR can't find a place for this agent: %s, so we're bailing\n", g_mon[agent].name);
      return -1;
    }
    ret = putAgentSpecificSpot(agent, x, y, facing);
  }while(ret != 0);

  return 0;
}

//Ret 0 on success, else you can't place it there
char putAgentSpecificSpot(agentIndex agent, int x, int y, direction_types facing)
{
  int equipX, equipY;

  if(agent == NO_AGENT || agent > g_numAgents)
  {
    logging( LOG_ERR, "ERR, trying to put a non-agent somewhere: %d\n", agent);
    return 1;
  }
  if(!inMap(x,y))
  {
    logging( LOG_ERR, "ERR, trying to put agent off map: %d %d\n", x,y);
    return 1;
  }

  g_mon[agent].x = x;
  g_mon[agent].y = y;
  g_mon[agent].facing = facing;

  //Find out where the equipment goes
  equipX = x;
  equipY = y;
  moveDir(agent, &equipX, &equipY, facing);
  if( g_map[x][y].tile != TILE_BLANK || g_map[equipX][equipY].tile != TILE_BLANK)
    return 1; //Failure

 //tODO clean
  g_map[x][y].agent = agent;
  g_map[x][y].tile = TILE_DUDE;
  g_map[equipX][equipY].tile = TILE_SWORD;

  return 0; //success
}






//bloody hell, there has GOT to be a slicker solution than this. Maybe I'm just tired.
// Doesn't run in the main loop, meh
agentIndex nearestAgent(int fromX, int fromY, int includingAtXY)
{
  int toX,toY;
  int min;
  int bestX, bestY;

  bestX = 0;
  bestY = 0;
  min = 50000;
  for(toX = 0; toX < MAP_SIZE_X; toX++)
  {
    for(toY = 0; toY < MAP_SIZE_Y; toY++)
    {
      if(!includingAtXY && toX == fromX && toY == fromY )
        continue;

      if( g_map[toX][toY].tile == TILE_DUDE )
      {
        if(abs(toX - fromX) + abs(toY - fromY) < min)
        {
          bestX = toX;
          bestY = toY;
          min = abs(toX - fromX) + abs(toY - fromY);
        }
      }
    }
  }
  return g_map[bestX][bestY].agent;
}


char lookForward( int x, int y, int facing)
{
  switch(facing)   
  {
    case NORTH:
      y = y-1;
      break;
    case SOUTH:
      y = y+1;
      break;
    case EAST:
      x = x+1;
      break;
    case WEST:
      x = x-1;
      break;
    default:
      logging(LOG_ERR ," Looking in an odd direction @%d,%d Direction: [0x%0X])\n", x,y,facing);
  }
  if( !inMap(x,y))
  {
    return TILE_OOB;
  }
  return g_map[x][y].tile;
}

//So... right is... positive. Yeah. Sure.   Damn sinister people are always so negative.
char lookAround(int x, int y, int facing, int forward, int rightLeft)
{
  switch(facing)   
  {
    case NORTH:
      y -= forward;
      x += rightLeft;
      break;
    case SOUTH:
      y += forward;
      x -= rightLeft;
      break;
    case EAST:
      x += forward;
      y += rightLeft;
      break;
    case WEST:
      x -= forward;
      y -= rightLeft;
      break;
    default:
      logging(LOG_ERR ," LookingAround in an odd direction @%d,%d Direction: [0x%0X], forward: %d rl: %d\n", x,y,facing, forward, rightLeft);
  }
  
  if( !inMap(x,y))
  {
    return TILE_OOB;
  }
  return g_map[x][y].tile;
}


