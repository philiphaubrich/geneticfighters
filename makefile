#So this is the fully automatic makefile that does ALLLLL sorts of stuff in the background under the hood.
# Specifically if it needs to make any .o file it assumes a default rule
# $(CC) $(CFLAGS) -c $@ $*.c
# $@ is the full target name
# $* is the target name with the suffix cut off, so they can add the .c

     #I want the project and .exe and whatnot to be the project name, not main. 
P       = geneticFighters
OBJECTS = gpfight.o gplib.o gpopcode.o logging.o display.o trial.o map.o mon.o aiAlgo.o
CFLAGS  = -std=gnu99 -g -Wall -pg
LIBS    = 
LDFLAGS = -lm -lcurses
WINFLAGS = -DWINDOWS_ENVIRONMENT -lmingw32 -lpdcurses
CC      = gcc

$(P): $(OBJECTS)
	$(CC) main.c $(OBJECTS) -o $(P).exe $(LDFLAGS) $(LIBS) $(CFLAGS) -Wl,-rpath,.


unit: $(P) unit$(P).o
	$(CC) $(P).c unit$(P).o -o unit$(P).exe -DTEST $(LDFLAGS) $(LIBS) $(CFLAGS)

unit$(P).o: unit$(P).c
	$(CC) unit$(P).c -c -DTEST $(LDFLAGS) $(LIBS) $(CFLAGS)

debug: CFLAGS += -DDEBUGVIEW
debug: $(P)

clean: 
	rm -f *.o
	rm -f $(P).exe
	rm -f unit$(P).exe
	rm -f log.txt
	rm -f chart.txt
	rm -f cscope.out
	rm -f agents/*

