#ifdef WINOWS_ENVIRONMENT
#include "curses.h"	//pdcurses for windows
#else
#include <ncurses.h>	//ncurses for linux
#endif

#include "gpfight.h"
#include "globals.h"

// Check if a point is in or out of the screen,
// Return 1 if good, 0 if bad
char inScreen(int row, int col)
{
  if( col < 0 || row < 0 || col > SCREEN_SIZE_X || row > SCREEN_SIZE_Y)
  {
    return 0;
  }
  return 1;
}


void paintMap() 
{
  //struct Agent* tmpAgent; 
  int x,y;

  for(x = 0; x < MAP_SIZE_X; x++)
  {
    for(y = 0; y < MAP_SIZE_Y; y++)
    {
      mvprintw(y+1,x,"%c",g_map[x][y].tile);
    }
  }

}

