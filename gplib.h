#ifndef __GPLIB__
#define __GPLIB__
/** @file GPLib header should be the interface between the domain application and the genetic programming aspect   
 * It should really handle fitness, selection, GP metrics, 
 *
 * Hey, so after some reading, this is "interpreted linear genetic programming"
 * And yeah, it's order of magnatude faster than those silly trees. 
 * Also, it's been done. Back in 1994...   shrug.
*/

#include <stdio.h>
#include "gpfight.h"
#include "globals.h"

//TODO see if you can eliminate some of these extern junk
extern FILE* f_chart;                             ///< for metrics. This should probably own it.
extern int g_numAgents;                           ///< Variable from arguments. Used for a variety of little things

//TODO: wtf is this rage shit?
//All of it
#define RAGESIZE  1000
extern struct Agent* g_philRAGEagent[RAGESIZE];
extern uint16* g_philRAGEmind[RAGESIZE];
extern int g_rageIndex;
//I think this is where I left off and one of the reasons I abandoned this project.
// Other than, you know, the kid

//TODO: Should probably have a #ifdef METRICS or something to bypass all this optional stuff
/// Metrics. I'm trying to figure out what works and what doesn't. 
struct FitnessMetric
{
  int medianFitness;
  int avgFitness;
  int maxFitness;
  int minFitness;
  int avgCycles;
  int maxCycles;
  int minCycles;
  int avgRounds;
  int maxRounds;
  
  int bredTop;
    
  //fitness of randomly generated
  //fitness of clones
  //fitness of bred
};


typedef enum {
 REWARD_KILL = 1000,
} REWARD;

//TODO: name prefix: gp   Try and keep this shit straight

/** Turns opcodes into human readable text. 
 * Used for RAW vs formated minds.   
 * BUT! It's not reversible. You LOSE information.  
 * And it hasn't been word-boundary strict, so it REALLY fucked up agents.
 * @param op  The opcode, first chunk of DNA. Also determins absolute or relativeness of arguments
 * @param arg1 second chunk of DNA
 * @param arg2 third chunk of DNA
 * @param ret  The formated string, one line of DNA
 */
void formatMind(char op, char arg1, char arg2, char* ret, int retSizeInBytes);

void saveAgent(agentIndex agent, FILE* f_save);  ///< Save agent to file.
agentIndex loadAgent(char* filename, FILE* f_file, placement_types placement); ///> load agent from file to g_mon memory
void deleteAgent(agentIndex agent); ///< Removes agent from g_mon, and deletes it's file
int loadAgentsFromFile(int upto, placement_types placement); ///< Loads agents from file. Win vs Linux shenanigians. 


// The meat of Genetic Algorithm functions. 

void breedAgents(agentIndex dad, agentIndex mom, agentIndex baby);
void mutateAgent(agentIndex agent);
void mutateName(char* name, int nameSizeInBytes);
void evaluatePopulationFitness(struct FitnessMetric* fit);
void selection(struct FitnessMetric* fit);
void chartlog(struct FitnessMetric* fit);
void generateRandMind(uint16* mind);
void loadMindRAW(FILE* f_file, uint16* mind);

void genName(agentIndex agent, char* name);
char isNameUnique(agentIndex agent, char name[]);
char isJump(uint16 op);

void gpRewardGoal(agentIndex agent, REWARD reward);
#endif
