#ifndef __GPMAP__
#define __GPMAP__
/** @file map.h
 * Handling the playing field
 * splitting stuff out of gpfight just to be a little cleaner
 */

#include "globals.h"
#include "mon.h"

/// Tiles. I stored them as the character to go the screen which might have been a mistake.
/// Also used as a return type of agents looking at things, loaded into AX reg
#define NUM_TILES 6
typedef enum 
{
  TILE_OOB    ='X',
  TILE_BLANK  =' ',
  TILE_WALL   ='#',
  TILE_DUDE   ='@',
  TILE_SWORD  ='!',
  TILE_SHIELD ='+'
    //  TILE_WATER  ='~',
}tile_types;


/// Types of mapes to make, the surrounding "terrain" if you will. Needs to be expanded into scenarios
//TODO: We need to build up the AI with baby steps. This was even in the original idea way back in 2011. I just lept ahead.
//    We need a set of simple challenges.
//  Empty room with swords around, don't kill yourself.
//  One fixed braindead peasant in a small room. Move there.
//  One randomly placed peasant in a small room. Kill it.
//  One randomly placed peasant in a small room with swords placed around.
//  One peasant that moves randomly.
//  One peasant that flees. 
//  Group of peasants flee.
typedef enum
{
  MAP_EMPTY,
  MAP_SWORD_PERIMITER,
  MAP_WALL_PERIMITER,
  MAP_RANDOM_WALLS,
  MAP_RANDOM_SWORDS,
  MAP_SMALLROOM,
}map_types;

#define SMALL_ROOM_SIZE 10





/// There's an map_size 2D array of this. It's nice and light
struct map   
{
  tile_types tile;  ///< the character printed
  agentIndex agent; ///< -1 for no-one, otherwise the index of the agent
};



//--------------------Globals, because fuck you

extern struct map g_map[MAP_SIZE_X][MAP_SIZE_Y];  ///< Used everywhere. Really just holds position and terrain now


//--------------------FUNCTIONS

char inMap(int row, int col);     ///< Simple check   TODO: change to isInMap
void generateMap(map_types mapType); ///< Sets the background terrain. Walls, swords, etc
int tileLookup(tile_types tile);   ///< Turning tiles into a number rather than a character
tile_types getTileType(int x, int y); ///< getter
/** returns agent orthagonally closests to x/y
 *  Preference for Left and top on a tie
 *  @param includingAtXY  If false, then it ignores the origin square. Like if that's an agent itself looking for others
 *  @return target agent
 */
agentIndex nearestAgent(int fromX, int fromY, int includingAtXY); 
char putAgentRandSpot(agentIndex i); ///< used for initial placement. Entire map area. Program dies if it fails.
char putAgentSpecificSpot(agentIndex i, int x, int y, direction_types facing);

char lookForward( int x, int y, int facing); ///< Handy shortcut.  (But for agents will always be what they're holding!)
char lookAround(int x, int y, int facing, int forward, int rightLeft); ///< Look around you. Relative to agent. 


#endif

