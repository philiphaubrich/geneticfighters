
#include "logging.h"
#include "gplib.h"
#include "map.h"
#include "globals.h"
#include "mon.h"

#include <stdlib.h>

struct Agent g_mon[MAX_NUM_AGENTS];               ///< fuck that dynamic shit. We're handing around indexes now. These are the dudes that perform actions and make up the population

int g_numAgents = NUM_AGENTS_DEFAULT;            ///< arg -a  How big of a population to maintain

/* Thinking about swords, shields and dudes. But, pft, I never got to shields
   2) Moves into        
         OOB      blank  wall      dude         sw           sh
   blank N/A      N/A     N/A      N/A          N/A          N/A
   wall  N/A      N/A     N/A      N/A          N/A          N/A
   dude  blocked  Enters  blocked  BothStunned  IsKilled     isStunned
   sw    blocked  Enters  blocked  kills        BothStunned  isStunned
   sh    blocked  Enters  blocked  Stuns        Stuns        BothStunned



   N/A  Move completes?          
   Enters  y          
   blocked  n          
   kills target  y          
   iskilled  y          
   stuns  n          
   isstunned  n          
   BothStunned  n 

*/
collision_effect_types collisionTable[NUM_TILES][NUM_TILES] = 
{
  //  OOB         blank       wall       dude       sword      shield  
  {NA,        NA,          NA,        NA,        NA,        NA},  //OOB
  {NA,        NA,         NA,        NA,        NA,        NA},  //Blank
  {NA,        NA,          NA,        NA,        NA,        NA},  //Wall
  {BLOCKED,   ENTERS,     BLOCKED,   BOTH_STUN, IS_KILLED, IS_STUNNED},  //Dude
  {BLOCKED,   ENTERS,     BLOCKED,   KILLS,     BOTH_STUN, IS_STUNNED},  //Sword
  {BLOCKED,   ENTERS,     BLOCKED,   STUNS,     STUNS,     BOTH_STUN}   //Shield
};



//load who you can to get g_numAgents up to target
//Generate agents to fill the gap. 
void initMon()
{
  int count, tomake;
  logging(LOG_HIGHGAME, "init Mon \n");
  count = loadAgentsFromFile( g_numAgents, PLACEMENT_NONE);
  tomake = g_numAgents - count;
  while(tomake --> 0)
  {
    generateAgent(PLACEMENT_NONE, 0,0,0, NO_AGENT, 0);
  }
}

void agentResetRound()
{
  for(int i=0; i<g_numAgents; i++)
  {
    g_mon[i].fitness = 0;
    //g_mon[i].active = 0; //Should be moot
    g_mon[i].cycles = 0;
    //g_mon[i].x = 0; //Moot
    //g_mon[i].y = 0;
  }
}

//Hmmm. could check the intervening squares between old location and new for blocking walls.
void agentRotate(agentIndex i, direction_types direction)
{
  if( i == NO_AGENT || i > g_numAgents)
    return;
  switch(direction)
  {
    default:
      //Hmmm. Yeah, movement should not rotate us. Side-stepping is standard.
      //      g_mon[i].facing = direction;
      break;
    case ROTATE_CW:
      g_mon[i].facing = (g_mon[i].facing + 1)%4;
      break;
    case ROTATE_CCW:
      g_mon[i].facing = (g_mon[i].facing - 1)%4;
  }
}


char agentMove(agentIndex movingAgent, direction_types direction)
{
  tile_types fromTile, toTile;
  int toX,toY;
  int equipX, equipY;
  char moveCompletes = 1;
  char revertAgentMove = 0;

  if(movingAgent == NO_AGENT)
  {
    logging(LOG_ERR, "ERR, why you trying to move someone that isn't there? %d\n", movingAgent);
    return 0;
  }

  int x = g_mon[movingAgent].x;
  int y = g_mon[movingAgent].y;

  if(direction >= REST)   //TODO strongly consider being more loose with valid output. Like divvy up the range of output and each is a potential move
    return 1;  

  //Remove sword, move agent, put down sword. 
  //  If sword can't go down, revert agent move
  //  If agent dies, sword disappears as non-object
  //   No pending actions, just one revert if the sword fails.
  equipX = x; 
  equipY = y;
  moveDir(movingAgent, &equipX, &equipY, g_mon[movingAgent].facing);
  if(g_map[equipX][equipY].tile != TILE_SWORD)
    logging(LOG_ERR ,"ERR, wtf. agent %s @%d,%d (moving %d,%d) doesn't have a sword at %d, %d. Tile ahead is a [%c]\n",
        g_mon[movingAgent].name,g_mon[movingAgent].x, g_mon[movingAgent].y, equipX, equipY, g_map[equipX][equipY].tile);
  g_map[equipX][equipY].tile = TILE_BLANK;

  //Moving the agent

  //get types
  fromTile = TILE_DUDE; //It's moving, it's implied it's a dude
  toX = x;
  toY = y;
  moveDir(movingAgent,&toX, &toY, direction);
  toTile = getTileType(toX,toY);

  if(x == toX && y == toY)
    toTile = TILE_BLANK;  //Such a lame hack. but if they rotate, then they're attempting to move into themselves....

  //Consult collision table and resolve effects
  switch(collisionTable[tileLookup(fromTile)][tileLookup(toTile)])
  {
    case KILLS:
      //addPending(IS_KILLED, toX, toX, direction, g_map[toX][toY].agent);
      //addPending(KILLS, x, y, direction, movingAgent);
      //addPending(ENTERS, toX, toY, direction, tmpAgent);
      //WTF, shouldn't happen
      break;
    case ENTERS:
      g_map[x][y].agent = NO_AGENT;
      g_map[x][y].tile = TILE_BLANK;
      g_map[toX][toY].agent = movingAgent;
      g_map[toX][toY].tile = TILE_DUDE;
      g_mon[movingAgent].x = toX;
      g_mon[movingAgent].y = toY;
      agentRotate(movingAgent, direction);

      break;
    case IS_KILLED:
      agentKill(movingAgent); //not killed just detained, agent still valid for now
      movingAgent = NO_AGENT;
      moveCompletes = 0;
      break;

    case BLOCKED:
      moveCompletes = 0;
      break;
    case STUNS:
      agentStun(g_map[toX][toY].agent);
      moveCompletes = 0;
      break;
    case IS_STUNNED:  
      agentStun(movingAgent);
      moveCompletes = 0;
      break;
    case BOTH_STUN:  
      agentStun(movingAgent);
      agentStun(g_map[toX][toY].agent);
      moveCompletes = 0;
      break;
    case NA:
    default:
      moveCompletes = 0;
      logging(LOG_ERR ,"ERR, bad collision: %d, fromTile: %d, toTile: %d, from: %d,%d to: %d,%d agent: %s\n",
          collisionTable[tileLookup(fromTile)][tileLookup(toTile)],
          tileLookup(toTile),
          tileLookup(fromTile),
          g_mon[movingAgent].x,
          g_mon[movingAgent].y,
          toX,
          toY,
          g_mon[movingAgent].name);
      //mvprintw(0,0,"ERR bad collision");
      return 0;

  }

  if(moveCompletes)
  {
    //Agent is moved, if sword can't complete move, revert
    //  (wasteful for REST actions, but meh)
    equipX = g_mon[movingAgent].x; 
    equipY = g_mon[movingAgent].y;
    moveDir(movingAgent, &equipX, &equipY, g_mon[movingAgent].facing);
    toTile = getTileType(equipX,equipY);

    //Consult collision table and resolve effects
    switch(collisionTable[tileLookup(TILE_SWORD)][tileLookup(toTile)])
    {
      case KILLS:
        agentKill(g_map[equipX][equipY].agent);
        g_map[equipX][equipY].tile = TILE_BLANK; //So, for non-agents, the kill function doesn't clear the tile
        g_mon[movingAgent].kills++;
        logging(LOG_MIDGAME ,"%s Makes a kill!\n",g_mon[movingAgent].name);
        gpRewardGoal(movingAgent, REWARD_KILL);
        break;
      case ENTERS:

        break;
      case IS_KILLED:
        //wtf? how did you kill a sword?
        break;

      case BLOCKED:
        revertAgentMove = 1;
        moveCompletes = 0;
        break;
      case STUNS:
        agentStun(g_map[toX][toY].agent);
        moveCompletes = 0;
        revertAgentMove = 1;
        break;
      case IS_STUNNED:  
        agentStun(movingAgent);
        moveCompletes = 0;
        revertAgentMove = 1;
        break;
      case BOTH_STUN:  
        agentStun(movingAgent);
        agentStun(g_map[toX][toY].agent);
        moveCompletes = 0;
        revertAgentMove = 1;
        break;
      case NA:
      default:
        moveCompletes = 0;
        logging(LOG_ERR ,"ERR, bad collision with SWORD: %d, fromTile: SWORD, toTile: %d, to: %d,%d agent: %s\n",
            collisionTable[tileLookup(fromTile)][tileLookup(toTile)],
            tileLookup(toTile),
            equipX,
            equipY,
            g_mon[movingAgent].name);
        //mvprintw(0,0,"ERR bad collision");
        return 0;  
    }
  }

  if(revertAgentMove) //Maybe not the most graceful, but fuckitall
  {
    //UNMOVE the agent
    g_map[x][y].agent = movingAgent;
    g_map[x][y].tile = TILE_DUDE;
    if( !( x == toX && y == toY))  //Like, if he rotated.
    {      
      g_map[toX][toY].agent = NO_AGENT;
      g_map[toX][toY].tile = TILE_BLANK;
    }
    g_mon[movingAgent].x = x;
    g_mon[movingAgent].y = y;
    //hmmmm
    if(direction == ROTATE_CCW)
      agentRotate(movingAgent, ROTATE_CW);
    if(direction == ROTATE_CW)
      agentRotate(movingAgent, ROTATE_CCW);
    equipX = x;
    equipY = y;
    moveDir(movingAgent,&equipX, &equipY,g_mon[movingAgent].facing);
  }

  //place down the sword
  if( g_map[equipX][equipY].tile != TILE_BLANK)
  {
    logging(LOG_ERR ,"Problem placing down a sword at %d-%d, it's not empty, it's a [%c] tile, Reverted? %d, moveComplete? %d, \n", equipX, equipY, g_map[equipX][equipY].tile, revertAgentMove, moveCompletes);
  }
  g_map[equipX][equipY].tile = TILE_SWORD;


  return moveCompletes;
}





void agentKill(agentIndex agent)
{
  if(agent == NO_AGENT)
    return; // I guess we're ok with dummies

  g_mon[agent].deaths++;
  g_mon[agent].active = 0;
  logging(LOG_MIDGAME ,"agent %s KILLED! @%d,%d fit:%d\n", g_mon[agent].name, g_mon[agent].x, g_mon[agent].y, g_mon[agent].fitness);   //in fact, none of this gets hit. it's all skipped!
  g_map[g_mon[agent].x][g_mon[agent].y].agent = NO_AGENT;
  g_map[g_mon[agent].x][g_mon[agent].y].tile = TILE_BLANK;  //TODO, so, this isn't having the effect I expected.  When we place down the sword there, it's not blank
  //But he still exists in g_mon
}

void agentStun(agentIndex agent)
{
  if(agent == NO_AGENT)
    return; // I guess we're ok with dummies
  //TODO, yeah, I removed this mechanic....   Maybe it'll make a comback. 
}



//Adjust the pointer
void moveDir(agentIndex agent, int* x, int* y, direction_types direction)
{
  int origX, origY;
  int deltaX,deltaY;

  if(agent == NO_AGENT || agent > g_numAgents)
  {
    logging( LOG_ERR, "ERR, trying to move a non-agent somewhere: %d\n", agent);
    return;
  }


  origX = g_mon[agent].x;
  origY = g_mon[agent].y;

  switch(direction)   
  {
    case NORTH:
      *y = *y-1;
      break;
    case SOUTH:
      *y = *y+1;
      break;
    case EAST:
      *x = *x+1;
      break;
    case WEST:
      *x = *x-1;
      break;
    case ROTATE_CW: //Around a pivot square. For equipment.
      deltaX = *x - origX;
      deltaY = *y - origY;
      *x = origX - deltaY;
      *y = origY + deltaX;      
      break;
    case ROTATE_CCW:
      deltaX = *x - origX;
      deltaY = *y - origY;
      *x = origX + deltaY;
      *y = origY - deltaX;
      break;
    case FORWARD:
      moveDir(agent, x,y, g_mon[agent].facing);
      break;
    case STRAFERIGHT:
      moveDir(agent, x,y, (g_mon[agent].facing+1)%4);
      break;
    case STRAFELEFT:
      moveDir(agent, x,y, (g_mon[agent].facing-1)%4);
      break;
    case BACKUP:
      moveDir(agent, x,y, (g_mon[agent].facing+2)%4);
      break;
    case REST:  //No action
      break;

    default:      
      logging(LOG_ERR, "ERR, #%d @ [%d][%d] trying to move %d, a non-direction \n", agent, x,y, direction);
      break;
  }
}





int headCount()
{
  int x, y;
  int pointers = 0;
  int dudes = 0;

  for(x = 0; x < MAP_SIZE_X; x++)
  {
    for(y = 0; y < MAP_SIZE_Y; y++)
    {
      if(g_map[x][y].agent != NO_AGENT)
        pointers++;
      if(g_map[x][y].tile == TILE_DUDE)
        dudes++;
    }
  }
  logging(LOG_VERBOSE,"%d dudes, %d pointers\n",dudes, pointers);
  return dudes;
}




agentIndex agentGetFirstOpenSlot()
{
  int i;
  for( i = 0; i < g_numAgents; i++)
  {
    if( g_mon[i].occupied == 0)
      return i; 
  }
  return NO_AGENT;
}

// It's just Fisher-yates
void shuffleMonList(int list[], int sizeOfList, int* numViable)
{
  int i;
  int viableCount=0;
  for( i=0; i < sizeOfList; i++)
  {
    if( g_mon[i].occupied)
    {
      list[viableCount++] = i;
    }
  }

  for( i = viableCount - 1; i > 0; i--) 
  {
    int randoPick = (rand() % (i+1));
    int t = list[randoPick];
    list[randoPick] = list[i];
    list[i] = t;
  }
  *numViable = viableCount;
}



