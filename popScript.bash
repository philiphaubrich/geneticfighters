#!/bin/bash 
#ok, so this script runs the program a number of times and find out how fast various population sizes can find the optimal solution
#CAVEAT: it depends on fitness to be able to reach ~4000, (and that being a decent place to call success
#CAVEAT: It's measuring REALTIME, not rounds or processing ticks. So it's tied to hardware. 

TIMEOUT=100000;

POPSIZE=10;
ATTEMPT=0;
AVG=0;

while [  $POPSIZE -lt 100 ]; do

  while [ $ATTEMPT -lt 10 ]; do
    rm -f popsize.log
    rm -f agents/*
    rm -f log.txt
    rm -f chart.txt
    STARTTIME=$SECONDS
    ./geneticFighters.exe -d -a $POPSIZE -r $TIMEOUT >> popsize.log 
    ENDTIME=$SECONDS
    if grep "lastBest: 4000" popsize.log -q; then
      echo "POPSIZE $POPSIZE takes $(($ENDTIME - $STARTTIME))s";
    else
      echo "POPSIZE $POPSIZE fail after $(($ENDTIME - $STARTTIME))s";
    fi
    let AVG=AVG+$(($ENDTIME - $STARTTIME))
    let ATTEMPT=ATTEMPT+1
  done
  echo "---POPSIZE $POPSIZE averages $(($AVG/10))";
  let POPSIZE=POPSIZE+20
  let ATTEMPT=0
  let AVG=0
done

#pop 5 just can't get a solution

#10,000 might not be enough. ALL population sizes only solved it 3 or 4 times out of 10. The bigger pops typically took more realtime as they got all that processing time done on the first round. 

#Then I want to test if transitioning populations perform any better. 
#   Get the best popsize, start with 1/2, 1/5 1/10th that population, and let it cruise for HALF the typical time and then bump it up to the ideal size and let it go.   If it beats the average time, SUCCESS
