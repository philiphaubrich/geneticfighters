#ifndef __GLOBALS__
#define __GLOBALS__

/// @file Globals is a place to put commonly  used defines which are used everywhere. It's mere existance is a failure of proper compartementalization.  TODO: If at all possible, move this shit down or off to it's own header/class

// Mind word-size
//#define uint unsigned int
//#define uchar unsigned char
#define uint16 unsigned short

///Play area where agents move around
#define MAP_SIZE_X 80
#define MAP_SIZE_Y 30

//screen real-estate. So we can have a HUD
#define SCREEN_SIZE_X 80
#define SCREEN_SIZE_Y 31


#define AGENTNAMESIZE 30
#define MAX_NUM_AGENTS 1000    ///< Switching to a set array. 

///Defaults which program arguments can overwrite
#define ROUND_LENGTH_DEFAULT 100
#define NUM_ROUNDS_DEFAULT 10
#define NUM_AGENTS_DEFAULT 40
#define PROCESS_LIMIT_DEFAULT 1000
#define AGENT_TRIALS_DEFAULT 1

//#define LOGGING_DEFAULT LOG_ERR
#define LOGGING_DEFAULT LOG_ERR | LOG_HIGHGAME | LOG_MIDGAME | LOG_FITNESS
//#define LOGGING_DEFAULT LOG_ALWAYS

///MIND details,  which should definately go into gplib
//#define MIND_SIZE 256 //size limit of out 8-bit world.
#define MIND_SIZE 32768 // breaking that 8-bit barrier. Gonna have to range-check a bunch of opcode stuff 
#define CODE_START  8
#define ADDRESS_PC  0
#define ADDRESS_AX  1

//  Prefixes
#define ARG1IMM 0x8000 // 0b1000000000000000 Arg1 is an immediate value, rather than a reference
#define ARG2IMM 0x4000 // 0b0100000000000000 Arg2 immediate


#define NUMOPCODES 21  //The rest are unsupported right now
            //TODO find out why setting this to 22 causes a seg fault.


//TODO settle on a naming convention. Probably capitalize these or something
//Kinda wanted this in mon.h, but we got into circular dependencies
typedef int agentIndex;   ///< Shouldn't ever be used above g_numAgents. And REALLY shouldn't get above MAX_NUM_AGENT
#define NO_AGENT -1

/// Arguments for making new agents. LoadAgent and generateAgent
//Here to settle circular dependency issue. hmmmm
typedef enum
{
  PLACEMENT_RAND,
  PLACEMENT_NONE,
  PLACEMENT_SPECIFIC,
  PLACEMENT_CENTER,
  PLACEMENT_CORNERS,
  PLACEMENT_WESTSIDE,
}placement_types;



// Some stuff from main.c.  I'm not sure how I feel about main.h
void quitProgram();


#endif
